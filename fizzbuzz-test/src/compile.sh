clear
echo Compiling..
echo
rm -f ../bin/fizzbuzztest/*.class
javac -Xlint -cp .:../ext/junit-platform-console-standalone-1.5.2.jar ./fizzbuzztest/*.java
mv ./fizzbuzztest/*.class ../bin/fizzbuzztest
echo Done!
echo
ls -l ../bin/fizzbuzztest/*.class
