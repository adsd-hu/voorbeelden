package fizzbuzztest;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

import java.lang.IllegalArgumentException;

public class FizzBuzz
{
    public static String fizzBuzz(int i) throws IllegalArgumentException
    {
        if (((i % 5) == 0) && ((i % 7) == 0)) {
            return "fizzbuzz";
        } else if ((i % 5) == 0) {
            return "fizz";
        } else if ((i % 7) == 0) {
            return "buzz";
        }
        throw new IllegalArgumentException(Integer.toString(i));
    }
}