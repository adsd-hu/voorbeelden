package fizzbuzztest;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

public class Program 
{
	public static void main(String[] args) 
	{
		System.out.println( "Java Programming! \n");

        if(args.length ==1 && args[0].equals("-runtester"))
        {
            // Run JUnit core engine
            Result result = JUnitCore.runClasses(Tester.class);

            for (Failure failure : result.getFailures())
            {
                System.out.println(failure.toString());
            }

            System.out.println("\n[!] Overall test result: " + result.wasSuccessful() + "\n");
        }
	}
}
