/**
 * System class to process system console basic I/O operations
 * with typescript and node.js in the operating systems terminal
 *
 * Synchronous Readline for interactively running to have a conversation 
 * with the user via a console(TTY).
 *
 * date: march 2021
 * author: g.veldman
 *
 * Configure the local npm of your project with:
 *    npm install readline-sync
 *    npm i --save-dev @types/node
 *
 * Usage:
 * In your typescript class import the System class in the same root as:
 *    import {System} from "./System";
 *
 */
export class System
{
	/**
	 * Returns the input given on the system console as a string
	 * Follow the instructions given for this class System 
	 *
	 * Usage:
	 * In your typescript class 
	 *   var firstName:string = System.consoleReadline("Your name: ");
	 *
	 * @param	textValue to display on the same line
	 * @return  input given on the system console after enter
	 */
	static consoleReadline(textValue: string):string 
	{
		const readline = require('readline-sync');
		return readline.question(textValue);
	}

	/**
	 * Writes the specified string value to the system console
	 *
	 * @param	textValue to display as a new line
	 */
	static consoleWriteline(writeString: string):void
	{
		console.log(writeString);
	}

}