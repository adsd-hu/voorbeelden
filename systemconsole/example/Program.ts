import {System} from "./System";

class Program {

	static main() 
	{
		var firstName:string = System.consoleReadline("\nWhat is your first name? ");
		var lastName:string = System.consoleReadline("What is your last name? ");
		var age:number = Number(System.consoleReadline("Your age: "));

		System.consoleWriteline("\n:-) Welcome to this typescript course " + firstName + " " + lastName + " [" + age + "]");
		System.consoleWriteline("Good luck!\n");
	}

}

// Start the application
Program.main();