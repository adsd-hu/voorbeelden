# SystemConsole

System class to process system console basic I/O operations with typescript and node.js in the operating systems terminal. Synchronous Readline for interactively running to have a conversation  with the user via a console(TTY).

For the first time run 'npm install' from the main folder of this project.