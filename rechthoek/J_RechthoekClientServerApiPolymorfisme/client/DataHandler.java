/* DataHandler class
 *
 * by G. Veldman
 */

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import java.util.ArrayList;


public class DataHandler
{

	/**
	 * Returns a typesafe ArrayList with Rechthoek objects.
	 *
 	 * @return Collection with Rechthoek objects as ArrayList
	 */
	public ArrayList<Figuur> getfiguurLijst()
	{
	   // Use a server JSON API data source
	   // This can be changed in any data source, like SQL or XML
       try
        {
        	String url = "http://127.0.0.1:8000/figures?type=all";
        	return parseJSONData(url);
        }
        catch (Exception e)
        {
            // An exception occured during XML parsing. Check your XML filelocation and structure.
        	// Instead of return a null, throw the proper (custom) Exception object to handle any
        	// exceptions that may occur
        	// Example code available by G. Veldman

        	System.err.println(e);

            return null;
        }
	}


	/**
	 * Processes specified data from the specified Xml datasource.
	 * Returns a typesafe ArrayList with Rechthoek objects.
	 *
 	 * @param  url     	   	   url location of the server as String
 	 * @return        		   Collection with Rechthoek objects as ArrayList
	 */
    private ArrayList<Figuur> parseJSONData (String url) throws
                                                        		ParseException,
                                                         		FileNotFoundException,
                                                         		IOException
    {
    	ArrayList<Figuur> figuurLijst = new ArrayList<Figuur>();

        JSONParser parser = new JSONParser();
        URL serverLocation = new URL(url); // URL to Parse
        URLConnection serverConnected = serverLocation.openConnection();
        BufferedReader rawData = new BufferedReader(new InputStreamReader(serverConnected.getInputStream()));
        String inputLine;

        while ((inputLine = rawData.readLine()) != null)
        {
            JSONArray jsonData = (JSONArray) parser.parse(inputLine);

            // Loop through each item
            for (Object jsonObject : jsonData)
            {
                JSONObject jsonRecord = (JSONObject) jsonObject;

                String type = (String) jsonRecord.get("type");

                if(type.equals("rechthoek"))
                {
                    Double x = Double.parseDouble((String)jsonRecord.get("x"));
                    Double y = Double.parseDouble((String)jsonRecord.get("y"));
                    String omschrijving = (String) jsonRecord.get("omschrijving");

                    Rechthoek rechthoek = new Rechthoek(x, y);
    			    			rechthoek.setOmschrijving(omschrijving);

    			    			figuurLijst.add(rechthoek);
                }
                else if(type.equals("vierkant"))
                {
                    Double x = Double.parseDouble((String)jsonRecord.get("x"));
                    String omschrijving = (String) jsonRecord.get("omschrijving");

                    Vierkant vierkant = new Vierkant(x);
                    vierkant.setOmschrijving(omschrijving);

                    figuurLijst.add(vierkant);
                }
                else if(type.equals("cirkel"))
                {
                    Double r = Double.parseDouble((String)jsonRecord.get("r"));
                    String omschrijving = (String) jsonRecord.get("omschrijving");

                    Cirkel cirkel = new Cirkel(r);
                    cirkel.setOmschrijving(omschrijving);

                    figuurLijst.add(cirkel);
                }
            }
        }

        rawData.close();

        return figuurLijst;
    }

}
