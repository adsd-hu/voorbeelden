// class Vierkant
//
// 2020 februari
// Author: G. Veldman
//

public class Vierkant extends Figuur
{
    // operations

    // Constructor
    public Vierkant(double x)
    {
        super(x, 0);
    }

    // Methods (functions)

    public double berekenOppervlakte()
    {
        // Calculate and return oppervlakte
        return (x * x);
    }
}