// Abstract class Figuur


public abstract class Figuur 
{
    // Constants
    public final double PI = Math.PI;

    // Instance variables or fields
    protected double x;
    protected double y;
    private String omschrijving;


    // Default Constructor
    public Figuur(double x, double y)
    {
        this.x = x;
        this.y = y;
    }

     /**
     * Set method for X
     */
    public void setX(double x)
    {
    	this.x = x;
    }

    /**
     * Get method for X
     */
    public double getX()
    {
        return x;
    }

    /**
     * Set method for Y
     */   
    public void setY(double y)
    {
    	this.y = y;
    }

    /**
     * Get method for Y
     */
    public double getY()
    {
        return y;
    }
 
    /**
     * Set method for omschrijving
     */   
    public void setOmschrijving(String omschrijving)
    {
    	this.omschrijving = omschrijving;
    }
   
    /**
     * Get method for omschrijving
     */ 
    public String getOmschrijving()
    {
    	return this.omschrijving;
    }

    // Declare polymorph method to be implemented by derived classes
    public abstract double berekenOppervlakte();    

 }

