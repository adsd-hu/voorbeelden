Node.js TypeScript server side data API using JSON

Requirements
-Install Node.js
-Install npm
-Install TypeScript

Run server
-From the server folder run 'npm install' for the first time
-Run 'node program.js'

-Test in browser: 'http://127.0.0.1:8000/figures?type=all'
 -You should see raw JSON data from the data source data.js in the data folder

-Run the Java-client or your own