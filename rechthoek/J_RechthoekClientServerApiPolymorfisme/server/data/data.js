const figurenData = [
  {
    'type': 'rechthoek',
    'omschrijving': 'Rechthoek 1',
    'x': '4',
    'y': '10',
  },
  {
    'type': 'rechthoek',
    'omschrijving': 'Rechthoek 2',
    'x': '5',
    'y': '7',
  },
  {
    'type': 'rechthoek',
    'omschrijving': 'Rechthoek 3',
    'x': '15',
    'y': '27',
  },
  {
    'type': 'rechthoek',
    'omschrijving': 'Rechthoek 4',
    'x': '12',
    'y': '4',
  },
  {
    'type': 'rechthoek',
    'omschrijving': 'Rechthoek 5',
    'x': '9',
    'y': '7',
  },
  {
    'type': 'vierkant',
    'omschrijving': 'Vierkant 1',
    'x': '4',
  },
  {
    'type': 'vierkant',
    'omschrijving': 'Vierkant 2',
    'x': '2'
  },
  {
    'type': 'cirkel',
    'omschrijving': 'Cirkel 1',
    'r': '7',
  },
  {
    'type': 'cirkel',
    'omschrijving': 'Cirkel 2',
    'r': '12'
  },
];

module.exports = figurenData;
