"use strict";
// A Simple server in Typescript and Express
// G. Veldman @ dec 2020
exports.__esModule = true;
exports.Server = void 0;
var express = require("express");
var DataHandler_1 = require("./DataHandler");
var Server = /** @class */ (function () {
    function Server(ipAddress, portNumber) {
        this.ipAddress = ipAddress;
        this.portNumber = portNumber;
        this.dataHandler = new DataHandler_1.DataHandler();
        this.expressServer = express();
    }
    Server.prototype.launchServer = function () {
        // Handle all incoming HTTP-methods
        this.processGetMethod();
        this.processPostMethod();
        this.processPutMethod();
        this.processDeleteMethod();
        // Handle general functionality 
        this.startListener();
        this.errorHandler();
    };
    Server.prototype.setIpAddress = function (ipAddress) {
        this.ipAddress = ipAddress;
    };
    Server.prototype.setPortNumber = function (portNumber) {
        this.portNumber = portNumber;
    };
    // ---
    // Private methods
    // ---
    Server.prototype.startListener = function () {
        var _this = this;
        // Start the server listener and print specified data to the console
        this.expressServer.listen(this.portNumber, this.ipAddress, function () {
            console.log("Server listening on " + _this.ipAddress + ": " + _this.portNumber + " for requests :-)");
        });
    };
    Server.prototype.errorHandler = function () {
        this.expressServer.use(function (error, request, response, next) {
            console.error("[X] " + error.message);
            response.status(500).send('An error has occurred: ' + error.message);
        });
    };
    Server.prototype.processGetMethod = function () {
        var _this = this;
        // Process the GET request and send a response
        this.expressServer.get('/', function (request, response) {
            throw new Error('Nothing to here..');
        });
        this.expressServer.get('/person', function (request, response) {
            response.send("You requested " + request.query.firstname + " " + request.query.lastname);
        });
        this.expressServer.get('/figures', function (request, response) {
            var queryJsonResult = _this.dataHandler.getFigurenLijstAsJson(request.query.type);
            // Send data as string reponse
            if (queryJsonResult != null) {
                response.send(queryJsonResult);
            }
        });
    };
    Server.prototype.processPostMethod = function () {
        // implementation follows
    };
    Server.prototype.processPutMethod = function () {
        // implementation follows		
    };
    Server.prototype.processDeleteMethod = function () {
        // implementation follows		
    };
    return Server;
}());
exports.Server = Server;
