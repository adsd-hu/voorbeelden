// A Simple server in Typescript and Express
// G. Veldman @ dec 2020
import * as figurenData from './data/data.js'; // Data source file of figuren data

export class DataHandler {

	constructor()
	{
		// Do some specifie constructor implementation
	}

	// Return the data as string in JSON notation
	// Source can be a JSON file or anothet, like a database
	public getFigurenLijstAsJson(figurenType: string) : string
   	{
		if (figurenType == "all") 
		{
			// Send data as string reponse
			return JSON.stringify(figurenData);
		}
		else if (figurenType != null)
		{
			var typeCollection = [];

			for (let i = 0; i < figurenData.length; i++) 
			{
				if (figurenType.toLowerCase() == figurenData[i].type.toLowerCase()) 
				{
					typeCollection.push(figurenData[i])
				}
			}

			return JSON.stringify(typeCollection);

		}
		else
		{
			throw new Error('Missing arguments for type, for example ?type=all');
			return null;
		}
		
	}

}