"use strict";
exports.__esModule = true;
exports.DataHandler = void 0;
// A Simple server in Typescript and Express
// G. Veldman @ dec 2020
var figurenData = require("./data/data.js"); // Data source file of figuren data
var DataHandler = /** @class */ (function () {
    function DataHandler() {
        // Do some specifie constructor implementation
    }
    // Return the data as string in JSON notation
    // Source can be a JSON file or anothet, like a database
    DataHandler.prototype.getFigurenLijstAsJson = function (figurenType) {
        if (figurenType == "all") {
            // Send data as string reponse
            return JSON.stringify(figurenData);
        }
        else if (figurenType != null) {
            var typeCollection = [];
            for (var i = 0; i < figurenData.length; i++) {
                if (figurenType.toLowerCase() == figurenData[i].type.toLowerCase()) {
                    typeCollection.push(figurenData[i]);
                }
            }
            return JSON.stringify(typeCollection);
        }
        else {
            throw new Error('Missing arguments for type, for example ?type=all');
            return null;
        }
    };
    return DataHandler;
}());
exports.DataHandler = DataHandler;
