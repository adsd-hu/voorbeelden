
// A Simple server in Typescript and Express
// G. Veldman @ dec 2020
//
// Run example in a client or webbrowser App
//	localhost:8000/
// 	localhost:8000/person?firstname=Lisa&lastname=Brown
//  localhost:8000/figures?type=all (or rechthoek)

import {Server} from "./Server";

// Create instances of Server as myServer with specified location
let myServer1 = new Server();
myServer1.setIpAddress("localhost");
myServer1.setPortNumber(8000);

//let myServer2 = new Server("192.168.178.25", 8080);

// Run myServer(s)
myServer1.launchServer();
//myServer2.launchServer();