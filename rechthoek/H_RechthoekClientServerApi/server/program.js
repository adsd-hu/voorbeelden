"use strict";
// A Simple server in Typescript and Express
// G. Veldman @ dec 2020
//
// Run example in a client or webbrowser App
//	localhost:8000/
// 	localhost:8000/person?firstname=Lisa&lastname=Brown
//  localhost:8000/figures?type=all (or rechthoek)
exports.__esModule = true;
var Server_1 = require("./Server");
// Create instances of Server as myServer with specified location
var myServer1 = new Server_1.Server();
myServer1.setIpAddress("localhost");
myServer1.setPortNumber(8000);
//let myServer2 = new Server("192.168.178.25", 8080);
// Run myServer(s)
myServer1.launchServer();
//myServer2.launchServer();
