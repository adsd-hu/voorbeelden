Java Client: Program, use of ArrayList and iterator, Rechthoek, and server data

Requirements
-Recent Java Development Kit as Java Standard Edition (Java SE) with version greater than 10
-Make sure that the 'bin' folder of the Java software is at your systems Path configuration without conflicts
-Verify that 'java -version' displays the recent installed Java version
-Verify that 'javac -version' displays the recent installed Java Compiler version
-JSON Simple api, like json-simple-1.1.1.jar, see /ext folder

Compile on terminal
-Navigate to the folder of Program.java
-Compile 'javac -cp '.:./ext/json-simple-1.1.1.jar' *.java'
-Verify that the *.class has been created without errors or warnings

Execute on terminal
-Navigate to the folder of Program.java
-Execute 'java -cp '.:./ext/json-simple-1.1.1.jar' Program'
-Verify that Program executes without any errors or warnings and with proper functionality

Use IDE
-In Eclipse or IntelliJ IDEA CE make a default Java-project
-In the project create the necessary class files
-Copy/paste the source code in the proper class files
-Configure the external JSON api file
-Use Build, Run or Debug to compile and execute