/*
 * Example code implementation of Rechthoek with:
 * -General coding and comments (working in progress)
 * -Abstraction (classification, later on more)
 * -Encapsulation
 * -Inheritance
 * -Type safe collections
 * -External data, like JSON
 * -Client-server (Java client and http server, like node.js and express)
 *
 * Implemented in Java
 *
 * by G. Veldman
 */
import java.util.ArrayList;


public class Program 
{
	public static void main(String[] args) 
	{
        System.out.println("\nWelcome to dynamic Rechthoek Collection Client-Server :-)\n");

        DataHandler dataHandler = new DataHandler();
        ArrayList<Rechthoek> rechthoekLijst = null;   

        rechthoekLijst = dataHandler.getRechthoekLijst();

        // Als rechthoekLijst niet 'null' is, dus geen object van bestaat
        // dit gebeurt als er een exception was in DataModel
        // later gaan we die exceptions netjes hier verwerken
        if(rechthoekLijst != null && rechthoekLijst.size() > 0)
        {
            // Iterate rechthoekLijst
            // Gebruik generic type Rechthoek and polymorph methods
            for(int index=0; index < rechthoekLijst.size(); index++)
            {   
                Rechthoek huidigRechthoek = rechthoekLijst.get(index);
                
                System.out.printf("Oppervlakte %s is: %.2f \n", 
                                   huidigRechthoek.getOmschrijving(),
                                   huidigRechthoek.berekenOppervlakte());
            }

            System.out.print("\n");
        }
        else
        {
            // Om hier te komen, voeg een extra karakter in een xml tag, verander data source etc.
            System.err.println("\nAn exception occured during data processing from the data source.");
            System.err.println("Please check the data source location and its structure.\n");
        }
	}

}
