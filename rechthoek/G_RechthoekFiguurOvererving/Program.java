// Program class
// see https://www.tutorialspoint.com/java/index.htm
//
// Intro 2020

public class Program
{
    private static Rechthoek rechthoek1;
    private static Rechthoek rechthoek2;
    private static Cirkel cirkel1;

    public static void main(String[] args)
    {
        // Create instance and handle (association)
        rechthoek1 = new Rechthoek(10, 5);
        rechthoek1.setOmschrijving("Rechthoek 1");

        rechthoek2 = new Rechthoek(7, 18);
        rechthoek2.setOmschrijving("Rechthoek 2");

        cirkel1 = new Cirkel(5);
        cirkel1.setOmschrijving("Cirkel 1");


        // Display result
        System.out.format("Oppervlakte van %s (%.2f, %.2f) is ",
                                rechthoek1.getOmschrijving(),
                                rechthoek1.getX(),
                                rechthoek1.getY() );
        System.out.println(rechthoek1.berekenOppervlakte());

        System.out.format("Oppervlakte van %s (%.2f, %.2f) is ",
                                rechthoek2.getOmschrijving(),
                                rechthoek2.getX(),
                                rechthoek2.getY() );
        System.out.println(rechthoek2.berekenOppervlakte());

        System.out.format("Oppervlakte van %s (%.2f) is ",
                                cirkel1.getOmschrijving(),
                                cirkel1.getR() );
        System.out.format("%.2f\n", cirkel1.berekenOppervlakte());  


        // Change r of the current circle
        cirkel1.setR(2);

        System.out.format("Oppervlakte van %s (%.2f) is ",
                                cirkel1.getOmschrijving(),
                                cirkel1.getR() );
        System.out.format("%.2f\n", cirkel1.berekenOppervlakte());                    
    }
}