// class Cirkel
//
// 2020 februari
// Author: G. Veldman
//

public class Cirkel
{
    // attributes
    private double PI = Math.PI;
    private double r = 0.0;
    private String omschrijving = "";

    // operations

    // Constructor
    public Cirkel(double r)
    {
        this.r = r;
    }

    // Methods (functions)

    public void setR(double r)
    {
        this.r = r;
    }

    public double getR()
    {
        return r;
    }

    public void setOmschrijving(String omschrijving)
    {
        this.omschrijving = omschrijving;
    }

    public String getOmschrijving()
    {
        return omschrijving;
    }

    public double berekenOppervlakte()
    {
        // Calculate and return oppervlakte
        return (PI * Math.pow(r, 2) ); // pi * r^2
    }
}