// Program class
// see https://www.tutorialspoint.com/java/index.htm
//
// Intro 2020

public class Program
{
	// Variabelen
	private static double x = 0.0;
	private static double y = 0.0;

	// Start functie/method van mijn app
	public static void main(String[] args)
	{
		// Vierkant
		x = 5;
		y = x;
		double oppervlakte = x * x;

		System.out.println("Mijn figuur is een vierkant met oppervlakte: " + oppervlakte);
	}
}