// Program class
// see https://www.tutorialspoint.com/java/index.htm
//
// Intro 2020

public class Program
{
    private static Rechthoek rechthoek1;

    public static void main(String[] args)
    {
        // Create instance and handle (association)
        rechthoek1 = new Rechthoek(10, 5);
        rechthoek1.setOmschrijving("Rechthoek 1");

        // Display result
        System.out.format("Oppervlakte van %s (%.2f, %.2f) is ",
                                rechthoek1.getOmschrijving(),
                                rechthoek1.getX(),
                                rechthoek1.getY());
        System.out.println(rechthoek1.berekenOppervlakte());
    }
}