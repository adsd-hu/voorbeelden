// A Simple server in Typescript and Express
// G. Veldman @ dec 2020

import * as figurenData from './data/data.js'; // Data source file of figuren data
import {DataService} from "./DataService";

export class MockDataService implements DataService {
	// return the mocked data from file
	public getFigurenLijstAsJson(figurenType: string, callback: (rows: any) => void)
   	{
        let jsonString = JSON.stringify(figurenData);
        callback(jsonString);
	}
}