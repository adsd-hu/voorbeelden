CREATE TABLE rechthoek(
id INTEGER PRIMARY KEY AUTOINCREMENT,
omschrijving VARCHAR(100),
x INT,
y INT
);

CREATE TABLE vierkant(
id INTEGER PRIMARY KEY AUTOINCREMENT,
omschrijving VARCHAR(100),
x INT
);

CREATE TABLE cirkel(
id INTEGER PRIMARY KEY AUTOINCREMENT,
omschrijving VARCHAR(100),
r INT
);

INSERT INTO rechthoek (omschrijving, x, y) VALUES ('Rechthoek 1', 4, 10);
INSERT INTO rechthoek (omschrijving, x, y) VALUES ('Rechthoek 2', 5, 7);
INSERT INTO rechthoek (omschrijving, x, y) VALUES ('Rechthoek 3', 15, 27);
INSERT INTO rechthoek (omschrijving, x, y) VALUES ('Rechthoek 4', 12, 4);
INSERT INTO rechthoek (omschrijving, x, y) VALUES ('Rechthoek 5', 9, 7);

INSERT INTO vierkant (omschrijving, x) VALUES ('Vierkant 1', 4);
INSERT INTO vierkant (omschrijving, x) VALUES ('Vierkant 2', 2);

INSERT INTO cirkel (omschrijving, r) VALUES ('Cirkel 1', 7);
INSERT INTO cirkel (omschrijving, r) VALUES ('Cirkel 2', 12);
