"use strict";
// A Simple server in Typescript and Express
// G. Veldman @ dec 2020
exports.__esModule = true;
exports.Server = void 0;
var express = require("express");
var Server = /** @class */ (function () {
    // constructor with dependency injection (DataService)
    function Server(dataService, ipAddress, portNumber) {
        this.dataService = dataService;
        this.ipAddress = ipAddress;
        this.portNumber = portNumber;
        this.expressServer = express();
    }
    Server.prototype.launchServer = function () {
        // Handle all incoming HTTP-methods
        this.processGetMethod();
        this.processPostMethod();
        this.processPutMethod();
        this.processDeleteMethod();
        // Handle general functionality 
        this.startListener();
        this.errorHandler();
    };
    Server.prototype.setIpAddress = function (ipAddress) {
        this.ipAddress = ipAddress;
    };
    Server.prototype.setPortNumber = function (portNumber) {
        this.portNumber = portNumber;
    };
    // ---
    // Private methods
    // ---
    Server.prototype.startListener = function () {
        var _this = this;
        // Start the server listener and print specified data to the console
        this.expressServer.listen(this.portNumber, this.ipAddress, function () {
            console.log("Server listening on " + _this.ipAddress + ": " + _this.portNumber + " for requests :-)");
        });
    };
    Server.prototype.errorHandler = function () {
        this.expressServer.use(function (error, request, response, next) {
            console.error("[X] " + error.message);
            response.status(500).send('An error has occurred: ' + error.message);
        });
    };
    Server.prototype.processGetMethod = function () {
        var _this = this;
        // Process the GET request and send a response
        this.expressServer.get('/', function (request, response) {
            throw new Error('Nothing to do here..');
        });
        this.expressServer.get('/figures', function (request, response) {
            var queryJsonResult = _this.dataService.getFigurenLijstAsJson(request.query.type, function (result) {
                if (result != null) {
                    response.send(result);
                }
            });
        });
    };
    Server.prototype.processPostMethod = function () {
        // implementation follows
    };
    Server.prototype.processPutMethod = function () {
        // implementation follows		
    };
    Server.prototype.processDeleteMethod = function () {
        // implementation follows		
    };
    return Server;
}());
exports.Server = Server;
