"use strict";
exports.__esModule = true;
exports.DatabaseService = void 0;
var sqlite3 = require("sqlite3");
var DatabaseService = /** @class */ (function () {
    function DatabaseService() {
        this.openDatabase();
    }
    // Return the data from the database as string in JSON notation
    DatabaseService.prototype.getFigurenLijstAsJson = function (figurenType, callback) {
        var sql = "SELECT * FROM rechthoek";
        var params = [];
        this.database.all(sql, params, function (err, rows) {
            if (err) {
                console.log("error in getFigurenLijst");
                return;
            }
            else {
                callback(rows);
            }
        });
    };
    DatabaseService.prototype.openDatabase = function () {
        var DBSOURCE = "./data/database.db";
        this.database = new sqlite3.Database(DBSOURCE, function (err) {
            if (err) {
                // Cannot open database
                console.error(err.message);
                throw err;
            }
            else {
                // create schema and populate database
                console.log('Connected to the SQLite database.');
            }
        });
    };
    return DatabaseService;
}());
exports.DatabaseService = DatabaseService;
