# remove and recompile Typescript files
rm -f *.js
tsc *.ts

# clear and recreate database
rm data/*.db
sqlite3 data/database.db < data/schema.sql
