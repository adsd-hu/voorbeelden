// A Simple server in Typescript and Express
// G. Veldman @ dec 2020

export interface DataService {
    getFigurenLijstAsJson(figurenType: string, callback: (rows: any) => void);
}