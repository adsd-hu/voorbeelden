// A Simple server in Typescript and Express
// G. Veldman @ dec 2020
import * as figurenData from './data/data.js'; // Data source file of figuren data
import * as sqlite3 from 'sqlite3';
import {DataService} from "./DataService";

export class DatabaseService implements DataService {
	public database: any;

	constructor()
	{
		this.openDatabase();
	}

	// Return the data from the database as string in JSON notation
	public getFigurenLijstAsJson(figurenType: string, callback: (rows: any) => void)
   	{
		let sql = "SELECT * FROM rechthoek";
        let params = []
        this.database.all(sql, params, (err, rows) => {
            if (err) {
                console.log("error in getFigurenLijst");
                return;
              } else {
                  callback(rows);
              }
        });
	}

	private openDatabase()
	{
        const DBSOURCE = "./data/database.db";
        this.database = new sqlite3.Database(DBSOURCE, (err) => {
            if (err) {
              // Cannot open database
              console.error(err.message)
              throw err
            } else {
                // create schema and populate database
                console.log('Connected to the SQLite database.')
            }
        });
    }
}