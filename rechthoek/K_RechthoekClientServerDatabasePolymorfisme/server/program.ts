
// A Simple server in Typescript and Express
// G. Veldman @ dec 2020
//
// Run example in a client or webbrowser App
//	localhost:8000/
// 	localhost:8000/person?firstname=Lisa&lastname=Brown
//  localhost:8000/figures?type=all (or rechthoek)

import {Server} from "./Server";
import {DatabaseService} from "./DatabaseService";
import {MockDataService} from "./MockDataService";

// Create instances of Server as myServer with specified location
let mockService = new MockDataService();
// let databaseService = new DatabaseService();
let myServer1 = new Server(mockService, "localhost", 8000);
// myServer1.setIpAddress("localhost");
// myServer1.setPortNumber(8000);

//let myServer2 = new Server("192.168.178.25", 8080);

// Run myServer(s)
myServer1.launchServer();
//myServer2.launchServer();