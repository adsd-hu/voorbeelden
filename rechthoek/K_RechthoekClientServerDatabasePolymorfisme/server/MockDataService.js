"use strict";
// A Simple server in Typescript and Express
// G. Veldman @ dec 2020
exports.__esModule = true;
exports.MockDataService = void 0;
var figurenData = require("./data/data.js"); // Data source file of figuren data
var MockDataService = /** @class */ (function () {
    function MockDataService() {
    }
    // return the mocked data from file
    MockDataService.prototype.getFigurenLijstAsJson = function (figurenType, callback) {
        var jsonString = JSON.stringify(figurenData);
        callback(jsonString);
    };
    return MockDataService;
}());
exports.MockDataService = MockDataService;
