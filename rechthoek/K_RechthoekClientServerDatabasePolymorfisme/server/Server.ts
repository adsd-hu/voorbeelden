// A Simple server in Typescript and Express
// G. Veldman @ dec 2020

import * as express from 'express';
import {DataService} from "./DataService";

export class Server {
	private ipAddress: string;
	private portNumber: number;
	private dataService: DataService;
	private expressServer;

	// constructor with dependency injection (DataService)
	constructor(dataService: DataService, ipAddress: string, portNumber: number)
	// constructor(dataService: DataService, ipAddress?: any, portNumber?: any)
	{
		this.dataService = dataService;
		this.ipAddress = ipAddress;
		this.portNumber = portNumber;
		this.expressServer = express();
	}


	public launchServer()
   	{
		// Handle all incoming HTTP-methods
		this.processGetMethod();

		this.processPostMethod();

		this.processPutMethod();

		this.processDeleteMethod();

		// Handle general functionality 
		this.startListener();

		this.errorHandler();
	}

	public setIpAddress(ipAddress: string)
	{
		this.ipAddress = ipAddress;
	}

	public setPortNumber(portNumber: number)
	{
		this.portNumber = portNumber;
	}	


	// ---
	// Private methods
	// ---

	private startListener()
	{
		// Start the server listener and print specified data to the console
		this.expressServer.listen(this.portNumber, this.ipAddress, () => {
			console.log("Server listening on " + this.ipAddress + ": " + this.portNumber + " for requests :-)")
		});
	}

	private errorHandler()
	{
		this.expressServer.use(function (error, request, response, next) {
			console.error("[X] " + error.message);
			response.status(500).send('An error has occurred: ' + error.message);
		});		
	}

	private processGetMethod()
	{
  		// Process the GET request and send a response
		this.expressServer.get('/', (request, response) => {
			throw new Error('Nothing to do here..');
		});

		this.expressServer.get('/figures', (request, response) => {
			
			let queryJsonResult = this.dataService.getFigurenLijstAsJson(request.query.type, (result) => {
				if (result != null) {
					response.send(result);
				}
			});
		});
	}

	private processPostMethod()
	{
		// implementation follows
	}

	private processPutMethod()
	{
		// implementation follows		
	}
	
	private processDeleteMethod()
	{
		// implementation follows		
	}	

}