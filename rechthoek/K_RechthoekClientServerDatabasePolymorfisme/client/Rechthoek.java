// class Rechthoek
//
// 2020 februari
// Author: G. Veldman
//

public class Rechthoek extends Figuur
{
    // operations

    // Constructor
    public Rechthoek(double x, double y)
    {
        super(x, y);
    }

    // Methods (functions)

    public double berekenOppervlakte()
    {
        // Calculate and return oppervlakte
        return (x * y);
    }
}