// class Cirkel
//
// 2020 februari
// Author: G. Veldman
//

public class Cirkel extends Figuur
{
    // operations

    // Constructor
    public Cirkel(double r)
    {
        super(r, 0);
    }

    // Methods (functions)

    public void setR(double r)
    {
        super.setX(r);
    }

    public double getR()
    {
        return getX();
    }

    public double berekenOppervlakte()
    {
        // Calculate and return oppervlakte
        return (PI * Math.pow(x, 2) ); // pi * r^2
    }
}