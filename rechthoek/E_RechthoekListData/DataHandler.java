import java.lang.Integer;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


public class DataHandler
{
	
	/**
	 * Returns a typesafe ArrayList with Rechthoek objects.
	 *
 	 * @return Collection with Rechthoek objects as ArrayList
	 */
	public ArrayList<Rechthoek> getRechthoekLijst()
	{
		// Use a XML data source
		// This can be changed in any data source, like SQL or JSON
        try 
        {
        	String xmlFile = "data/rechthoeken.xml";
            return parseXmlData(xmlFile);
        } 
        catch (Exception e)
        {
            // An exception occured during XML parsing. Check your XML filelocation and structure.
        	// Instead of return a null, throw the proper (custom) Exception object to handle any
        	// exceptions that may occur
        	// Example code available by G. Veldman
        	
        	System.err.println(e);
            
            return null;
        }		

	}

	/**
	 * Processes specified data from the specified Xml datasource.
	 * Returns a typesafe ArrayList with Rechthoek objects.
	 *
 	 * @param  xmlFile Xml file location as String
 	 * @return         Collection with Rechthoek objects as ArrayList
	 */
    private ArrayList<Rechthoek> parseXmlData (String xmlFile) throws
                                                        		ParserConfigurationException,
                                                         		SAXException,
                                                         		IOException 
    {
    	ArrayList<Rechthoek> rechthoekLijst = new ArrayList<Rechthoek>();

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		// Load the input XML document, parse it
		Document document = builder.parse(new File(xmlFile));
		NodeList nodeList = document.getDocumentElement().getChildNodes();


		// Iterate through xml data nodes
		for (int i = 0; i < nodeList.getLength(); i++) 
		{
			Node node = nodeList.item(i);

			// Get specified data based on supported figuurType
			if (node.getNodeType() == Node.ELEMENT_NODE) 
			{
			    Element element = (Element) node;

			    // Get the value of the attributes
			    String figuurType = node.getAttributes().getNamedItem("type").getNodeValue();


			    if(figuurType.equals("Rechthoek"))
			    {
			      // Properties of figuur Vierkant
			      Double x = Double.parseDouble(element.getElementsByTagName("X")
			                          .item(0).getChildNodes().item(0).getNodeValue());

			      Double y = Double.parseDouble(element.getElementsByTagName("Y")
			                          .item(0).getChildNodes().item(0).getNodeValue());			      

			      String omschrijving = element.getElementsByTagName("Omschrijving")
			                          .item(0).getChildNodes().item(0).getNodeValue();

			      Rechthoek rechthoek = new Rechthoek(x, y);
			      rechthoek.setOmschrijving(omschrijving);     

			      rechthoekLijst.add(rechthoek);
			    }

			}
		}
        
        return rechthoekLijst;
    }

}