/*
 * Example code implementation of Rechthoek with:
 * -General coding and comments (working in progress)
 * -Encapsulation
 * -Type safe collections
 * -External data, like XML
 *
 * Implemented in Java
 *
 * See doc folder for the class diagram
 *
 * by G. Veldman
 */
import java.util.ArrayList;

public class Program 
{
	public static void main(String[] args) 
	{
        DataHandler dataHandler = new DataHandler();
        ArrayList<Rechthoek> rechthoekLijst = dataHandler.getRechthoekLijst();
        
        System.out.println("\nWelcome to dynamic Rechthoek Collection :-)\n");

        // Als rechthoekLijst niet 'null' is, dus geen object van bestaat
        // dit gebeurt als er een exception was in dataHandler
        // later gaan we die exceptions netjes hier verwerken
        if(rechthoekLijst != null)
        {
            // Iterate rechthoekLijst
            // Gebruik generic type Rechthoek and polymorph methods
            for(int index=0; index < rechthoekLijst.size(); index++)
            {   
                Rechthoek huidigRechthoek = rechthoekLijst.get(index);
                
                System.out.printf("Oppervlakte %s is: %.2f \n", 
                                   huidigRechthoek.getOmschrijving(),
                                   huidigRechthoek.berekenOppervlakte());
            }
        }
        else
        {
            // Om hier te komen, voeg een extra karakter in een xml tag, verander data source etc.
            System.err.println("An exception occured during data processing from the data source.");
            System.err.println("Please check the data source location and its structure.\n");
        }
	}

}
