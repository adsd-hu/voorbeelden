// Program class
//
// ArrayList simplest form
// https://www.w3schools.com/java/java_arraylist.asp
//
// Intro 2020

import java.util.ArrayList;

public class Program
{
    private static Rechthoek rechthoek1;
    private static Rechthoek rechthoek2;
    private static Rechthoek rechthoek3;
    private static ArrayList<Rechthoek> rechthoekLijst;

    public static void main(String[] args)
    {
    	// Type safe ArrayList of type Rechthoek
    	rechthoekLijst = new ArrayList<Rechthoek>();

        // Create instance and handle (association)
        rechthoek1 = new Rechthoek(10, 5);
        rechthoek1.setOmschrijving("Rechthoek 1");
        rechthoekLijst.add(rechthoek1);

        rechthoek2 = new Rechthoek(7, 18);
        rechthoek2.setOmschrijving("Rechthoek 2");
        rechthoekLijst.add(rechthoek2);

        rechthoek3 = new Rechthoek(7, 3);
        rechthoek3.setOmschrijving("Rechthoek 3");
        rechthoekLijst.add(rechthoek3);


        System.out.println("\nGebruik klassieke for-loop");

        // Itereer door de rechthoeklijst met een klassieke foor-loop
        for(int index = 0; index < rechthoekLijst.size(); index++)
        {
        	// Haal volgende rechthoek object uit de lijst
        	Rechthoek huidigeRechthoek = rechthoekLijst.get(index);

	        // Display result
	        System.out.format("Oppervlakte van %s (%.2f, %.2f) is ",
	                                huidigeRechthoek.getOmschrijving(),
	                                huidigeRechthoek.getX(),
	                                huidigeRechthoek.getY() );
	        System.out.println(huidigeRechthoek.berekenOppervlakte());
    	}


        System.out.println("\nGebruik iterator");

        // Itereer door de rechthoekLijst met een iterator
    	for(Rechthoek huidigeRechthoek: rechthoekLijst)
        {
	        System.out.format("Oppervlakte van %s (%.2f, %.2f) is ",
                        huidigeRechthoek.getOmschrijving(),
                        huidigeRechthoek.getX(),
                        huidigeRechthoek.getY() );
	        System.out.println(huidigeRechthoek.berekenOppervlakte());
        }

                    
    }
}