Java class: Program, use of ArrayList and iterator, Rechthoek

Requirements
-Recent Java Development Kit as Java Standard Edition (Java SE) with version greater than 10
-Make sure that the 'bin' folder of the Java software is at your systems Path configuration without conflicts
-Verify that 'java -version' displays the recent installed Java version
-Verify that 'javac -version' displays the recent installed Java Compliler version

Compile on terminal
-Navigate to the folder of Progam.java
-Compile 'javac *.java'
-Verify that Program.class has been created without errors or warnings

Execute on terminal
-Navigate to the folder of Progam.java
-Execute 'java Program'
-Verify that Program executes without any errors or warnings and with proper functionality

Use IDE
-In Eclipse or IntelliJ IDEA CE make a default Java-project
-In the project create the neccesary classfiles
-Copy/paste the source code in the proper classfiles
-Use Build, Run or Debug to compile and execute