// class TaalLabels
// Store languages speciefied and supported UI labels
//
// 2020 februari
// Author: G. Veldman
//

public class TaalLabels
{
    // attributes
    private String labelWelcome = "";
    private String labelArea = "";


    // Methods (functions)
    public void setLabelWelcome(String text)
    {
        labelWelcome = text;
    }

    public String getLabelWelcome()
    {
        return labelWelcome;
    }


    public void setLabelArea(String text)
    {
        labelArea = text;
    }

    public String getLabelArea()
    {
        return labelArea;
    }    
}