/*
 * Example code implementation of Rechthoek with:
 * -General coding and comments (working in progress)
 * -Abstraction
 * -Encapsulation
 * -Inheritance
 * -Polymorphism by using an abstract class
 * -Constructor overloading
 * -Method/function overriding
 * -Type safe collections
 * -External data, like XML
 *
 * Implemented in Java
 *
 * See doc folder for the class diagram
 *
 * by G. Veldman
 */
import java.util.ArrayList;

public class Program 
{
	public static void main(String[] args) 
	{
        String geslecteerdeTaal = "";

        // Haal gespecificeerde taal op
        if(args.length ==1)
        {
            geslecteerdeTaal = args[0];
        }
        else
        {
            geslecteerdeTaal = "Nederlands"; // standaard taal wanneer niets gespecificeerd
        }

        DataHandler dataHandler = new DataHandler();
        ArrayList<Rechthoek> rechthoekLijst = dataHandler.getRechthoekLijst();
        TaalLabels taalLabels = dataHandler.getTaalLabels(geslecteerdeTaal);

        // Als rechthoekLijst niet 'null' is, dus geen object van bestaat
        // dit gebeurt als er een exception was in dataHandler
        // later gaan we die exceptions netjes hier verwerken
        if(rechthoekLijst != null && taalLabels != null )
        {
            System.out.println("\n"+ taalLabels.getLabelWelcome() +"\n");

            // Iterate rechthoekLijst
            // Gebruik generic type Rechthoek and polymorph methods
            for(int index=0; index < rechthoekLijst.size(); index++)
            {   
                Rechthoek huidigRechthoek = rechthoekLijst.get(index);
                
                System.out.printf("%s "+ taalLabels.getLabelArea() +": %.2f \n", 
                                   huidigRechthoek.getOmschrijving(),
                                   huidigRechthoek.berekenOppervlakte());
            }
        }
        else
        {
            // Om hier te komen, voeg een extra karakter in een xml tag, verander data source etc.
            System.err.println("\nAn exception occured during data processing from the data source.");
            System.err.println("Please check the data source location and its structure.\n");
        }
	}

}
