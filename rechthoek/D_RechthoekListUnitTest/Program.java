// Program class
//
// ArrayList simplest form
// https://www.w3schools.com/java/java_arraylist.asp
//
// Intro 2020

import java.util.ArrayList;

public class Program
{
    private static Rechthoek rechthoek1;
    private static Rechthoek rechthoek2;
    private static Rechthoek rechthoek3;
    private static ArrayList<Rechthoek> rechthoekLijst;

    public static void main(String[] args)
    {
        if(args.length ==1 && args[0].equals("-runtester"))
        {
            Tester tester = new Tester();
            System.out.println( "\n[!] Overall test result: " + tester.runTester() + "\n");
        }
        else
        {        
        	// Type safe ArrayList of type Rechthoek
        	rechthoekLijst = new ArrayList<Rechthoek>();

            // Create instance and handle (association)
            rechthoek1 = new Rechthoek(10, 5);
            rechthoek1.setOmschrijving("Rechthoek 1");
            rechthoekLijst.add(rechthoek1);

            rechthoek2 = new Rechthoek(7, 18);
            rechthoek2.setOmschrijving("Rechthoek 2");
            rechthoekLijst.add(rechthoek2);

            rechthoek3 = new Rechthoek(7, 3);
            rechthoek3.setOmschrijving("Rechthoek 3");
            rechthoekLijst.add(rechthoek3);


            System.out.println("\nDisplay data:");

            // Itereer door de rechthoekLijst met een iterator
        	for(Rechthoek huidigeRechthoek: rechthoekLijst)
            {
    	        System.out.format("Oppervlakte van %s (%.2f, %.2f) is ",
                            huidigeRechthoek.getOmschrijving(),
                            huidigeRechthoek.getX(),
                            huidigeRechthoek.getY() );
    	        System.out.println(huidigeRechthoek.berekenOppervlakte());
            }
        }
                    
    }
}