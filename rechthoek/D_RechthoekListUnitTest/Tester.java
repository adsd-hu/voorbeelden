/*
 * In this class some unit test cases are defined
 */

public class Tester
{
    private Boolean overallTestResult = true;


    // Run the tester
    public Boolean runTester()
    {
        System.out.println("\nRun tester..");


        // Execute and display results of the defined unittests

        // Testcase 1, step-by-step written
        int testCase = 1;
        Boolean testResult = testCase1();
        printTestResult(testCase, testResult);


        // Testcase 2, short written
        printTestResult(2, testCase2()); 


         // Testcase 3              
        printTestResult(3, testCase3());


        return overallTestResult;
    }



    //=== START Define some unit test on level of methods

    // Test berekenOppervlakte()
    // Result should be 50.0
    private Boolean testCase1()
    {
        // Step-by-step written
        //
        Rechthoek figuur = new Rechthoek(10, 5);
        // Make a Double object to compare its value later
        Double oppervlakte = Double.valueOf( figuur.berekenOppervlakte() );
        // Execute test and save the test result
        Boolean testResult = oppervlakte.equals(50.0);
        
        // Evaluate the result on the overall test result
        testResult = setOverallTestResult(testResult);

        // Return the result of this testcase
        return testResult;
    }


    // Test berekenOppervlakte()
    // Result should be 16.0
    private Boolean testCase2()
    {
        // Short written
        //
        Rechthoek figuur = new Rechthoek(2, 8);

        return setOverallTestResult( Double.valueOf( figuur.berekenOppervlakte() ).equals(16.0) );
    }


    // Test setOmschrijving()/getOmschrijving()
    // Result should be "Rechthoek"
    private Boolean testCase3()
    {
        Rechthoek figuur = new Rechthoek(1, 1);
        figuur.setOmschrijving("Rechthoek"); 

        return setOverallTestResult(String.valueOf( figuur.getOmschrijving() ).equals("Rechthoek") );        
    }

    //=== END




    // Set overallTestResult to false when a testCase fails
    private Boolean setOverallTestResult(Boolean testResult)
    {
        if(!testResult)
        {
            overallTestResult = false;
        }

        return testResult;
    }


    // Print the test results
    private void printTestResult(int testCase, Boolean testResult)
    {
        System.out.println("\t#Test result @testCase " + String.valueOf(testCase) + ": " + testResult);
    }

}
