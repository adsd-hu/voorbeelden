import java.util.Locale;
import java.util.ResourceBundle;
public class InternationalizationDemo 
{

	public static void main(String[] args) 
	{
		ResourceBundle bundle;
		/*
		 * File name 'MessageBundle', locale '_en' and extension '.properties'
		 */

		// English
		bundle = ResourceBundle.getBundle("MessageBundle", Locale.ENGLISH);
		System.out.println("Label 'greeting' as: " + bundle.getString("greeting"));

		// German
		bundle = ResourceBundle.getBundle("MessageBundle", Locale.GERMAN);
		System.out.println("Label 'greeting' as: " + bundle.getString("greeting"));

		// Dutch
		Locale.setDefault(new Locale("nl"));
		bundle = ResourceBundle.getBundle("MessageBundle");
		System.out.println("Label 'greeting' as: " + bundle.getString("greeting"));
	}

}