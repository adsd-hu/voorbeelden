<?php
include_once('figuur.class.php');


class Vierkant extends Figuur
{	
	function __construct($x)
	{
		// Use parent constructor code implementation
		parent::__construct($x, 0);
	}
	
	// Implemented polymorph method from base class
	public function berekenOppervlakte()
	{
		return $this->x * $this->x;
	}
	
	// Default read-only properties
	public function getX()
	{
		return $this->x;
	}
	
}
