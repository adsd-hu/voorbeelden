<?php

abstract class Figuur
{
	// Instance variables or fields
	protected $x;
	protected $y;
	private $omschrijving;
	private $PI;
	
	// Overload Constructor does not exist in PHP
	// Constructor
	public function __construct($x, $y)
	{
		$this->x = $x;
		$this->y = $y;
		$this->PI = pi();
	}
	
	// Function to set omschrijving
	public function setOmschrijving($omschrijving)
	{
		$this->omschrijving = $omschrijving;
	} 
	
	// Function to get omschrijving
	public function getOmschrijving()
	{
		return $this->omschrijving;
	}
	
	// Read only customized PI value
	public function getPI()
	{
		return $this->PI;
	}
	
	public function setX($x)
	{
		$this->x = $x;
	}
	
	public function setY($y)
	{
		$this->y = $y;
	}	
	
	// Declare polymorph method to be implemented by derived classes
	public abstract function berekenOppervlakte();		
}
?>