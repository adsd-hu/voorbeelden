<?php
include_once('figuur.class.php');

/*
 * Class cilinder inherents from Figuur.
 * Use height H and radias R
 */
class Cilinder extends Figuur
{
	function __construct($h, $r)
	{
		// Use parent constructor code implementation
		parent::__construct($h, $r);
	}
	
	// Implemented polymorph method from base class
	public function berekenOppervlakte()
	{
		return (2 * $this->getPI() * $this->x * $this->x) + (2 * $this->getPI() * $this->x * $this->y);
	}
	
	// Default read-only properties
	public function getH()
	{
		return $this->x;
	}
	
	public function getR()
	{
		return $this->y;
	}
}
