<?php
/*
 * FigurenLijst inherents from ArrayObject to behave as a type safe collection list
 */

class FigurenLijst
{
	// Declare private collection
	private $collection;
	
	function __construct()
	{
		// Create collection
		$this->collection = new ArrayObject();
	}
	
	
	// Add specified Figuur to the collection
	public function addFiguur(Figuur $value)
	{
		// Call to parent class ArrayObject
		$this->collection->append($value);
	}
	
	// Remove the specified Figuur from the collection
	public function removeFiguur(Figuur $value)
	{
		// Copy content of the collection to $array, 
		// remove the specified value from this array, 
		// copy the content of the array back to the collection
		// free array
		for($index=0; $index < $this->collection->count(); $index++)
		{
			// Check equallity
			if($this->collection[$index] === $value)
			{
				$array = $this->collection->getArrayCopy();
				//use array_splice to remove the right, one element
				array_splice($array, $index, 1);
				$this->collection->exchangeArray($array);
				$array = null;
			}	
		}
	}
	
	// Return specified Figuur
	public function getFiguur($index)
	{
		return $this->collection[$index];
	}
	
	// Clear the collection
	public function clearAll()
	{
		// Copy content of an empty array in the collection
		$this->collection->exchangeArray(array());
	}
	
	// Return the current size of the collection
	public function sizeOf()
	{
		return $this->collection->count();
	}
}

?>