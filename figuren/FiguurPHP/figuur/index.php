<?php
/*
 * Example code implementation of Figuur with:
 * -General coding and comments (working in progress)
 * -Abstraction
 * -Encapsulation
 * -Inheritance
 * -Polymorphism by using an abstract class
 * -Constructor overloading
 * -Method/function overriding
 * -Type safe collections
 *
 * Implemented in Php
 *
 * See doc folder for the class diagram
 *
 * by G. Veldman
 */
include_once('figuur.class.php');
include_once('figurenlijst.class.php');
include_once('vierkant.class.php');
include_once('cilinder.class.php');

echo "PHP OO Programming! <br /><br />";

$figurenLijst = new FigurenLijst();

$figuur1 = new Vierkant(null);
$figuur1->setX(5);
$figuur1->setOmschrijving("Vierkant");
$figurenLijst->addFiguur($figuur1);

$figuur2 = new Cilinder(10,3);
$figuur2->setOmschrijving("Cilinder");
$figurenLijst->addFiguur($figuur2);


echo "Figuur 1 ".$figuur1->getOmschrijving().": ".$figuur1->getX()." bij ".$figuur1->getX()."<br />";

echo "Figuur 2 ".$figuur2->getOmschrijving().": ".$figuur2->getH()." hoog en straal ".$figuur2->getR()."<br />";

// Iterate figurenLijst
// Gebruik generic type Figuur and polymorph methods
for($index=0; $index < $figurenLijst->sizeOf(); $index++)
{
	$huidigFiguur = $figurenLijst->getFiguur($index);

	echo "Oppervlakte ".$huidigFiguur->getOmschrijving()." is: ".round($huidigFiguur->berekenOppervlakte(), 2);
	echo "<br />";

}
?>