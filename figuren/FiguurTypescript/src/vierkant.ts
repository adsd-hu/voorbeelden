// Class Vierkant
import { Figuur } from "./Figuur";

export class Vierkant extends Figuur { 
   

   //constructor overloading
   constructor();
   constructor(x :number);
   constructor(x?: any)
   { 
      super(x,0);
   }  

   // functions
   public getX() :number 
   {
      return this.x;
   }


   public berekenOppervlakte() :number
   {
      return this.x * this.x;
   }

} 