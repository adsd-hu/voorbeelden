import { Figuur } from "./Figuur";

// Class FigurenLijst

export class FigurenLijst 
{
    private items: Array<Figuur>;

    constructor() 
    {
        this.items = [];
    }


    public addFiguur(value: Figuur) :void 
    {
        this.items.push(value);
    }

    public removeFiguur(value: Figuur)
    {
        // ..
    }

    public getFiguur(index :number) :Figuur 
    {
        return this.items[index];
    }

    public clearAll()
    {
        // ..
    }

    public sizeOff(): number
    {
        return this.items.length;
    }
}