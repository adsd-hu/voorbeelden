// Class Cilinder
import { Figuur } from "./Figuur";

export class Cilinder extends Figuur { 
   

   //constructor overloading
   constructor();
   constructor(h :number, r :number);
   constructor(h?: any, r?: any)
   { 
      super(h, r);
   }  

   // functions
   public getH() :number 
   {
      return this.x;
   }


   public getR() :number 
   {
      return this.y;
   }


   public berekenOppervlakte() :number
   {
      return (2 * this.PI * this.x * this.x) + (2 * this.PI * this.x * this.y);
   }

} 