 //
 // Example code implementation of Figuur with:
 // -General coding and comments (working in progress)
 // -Abstraction
 // -Encapsulation
 // -Inheritance
 // -Polymorphism by using an abstract class
 // -Constructor overloading
 // -Method/function overriding
 // -Type safe collections
 //
 // Implemented in Typescript
 //
 // See doc folder for the class diagram
 //
 // by G. Veldman
 //

import {FigurenLijst} from "./FigurenLijst";
import {Figuur} from "./Figuur";
import {Vierkant} from "./Vierkant";
import {Cilinder} from "./Cilinder";


// Declare
var figurenLijst = new FigurenLijst();
var figuur1 = new Vierkant(5);
var figuur2 = new Cilinder(10, 3);


// Cerate objects
figuur1.setOmschrijving("Vierkant 1");
figuur2.setOmschrijving("Cilinder 1");


// Add objects to the list
figurenLijst.addFiguur(figuur1);
figurenLijst.addFiguur(figuur2);


// Iterator through list
for (var index = 0; index < figurenLijst.sizeOff(); index++ ) 
{
	var figuur :Figuur;
	figuur = figurenLijst.getFiguur(index);

	console.log("Figuur omschrijving: " + figuur.getOmschrijving() );
	console.log("Figuur oppervlakte:  " + figuur.berekenOppervlakte() + "\n");
}