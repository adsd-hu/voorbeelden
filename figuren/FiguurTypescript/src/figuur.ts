// Class Figuur

export abstract class Figuur { 
   // attributes 
   public readonly PI :number = Math.PI; 
   protected x :number;
   protected y :number;
   private omschrijving :string;
   

   //constructor 
   constructor(x :number, y :number) 
   { 
      this.x = x;
      this.y = y;
   }  

   // functions
   public setX(x: number) 
   {
      this.x = x;
   }


   public setY(y: number) 
   {
      this.y = y;
   }


   public setOmschrijving(omschrijving :string)
   {
      this.omschrijving = omschrijving;
   }  


   public getOmschrijving() :string 
   {
      return this.omschrijving;
   }
   

   // Implementation by derived class
   public abstract berekenOppervlakte() :void

} 