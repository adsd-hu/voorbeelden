echo "Compile ts files to js files"
echo

rm -f ./bin/*.js
tsc ./src/*.ts
cp ./src/*.js ./bin
rm -f ./src/*.js