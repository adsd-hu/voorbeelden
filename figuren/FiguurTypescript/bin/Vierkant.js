"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
// Class Vierkant
var Figuur_1 = require("./Figuur");
var Vierkant = /** @class */ (function (_super) {
    __extends(Vierkant, _super);
    function Vierkant(x) {
        return _super.call(this, x, 0) || this;
    }
    // functions
    Vierkant.prototype.getX = function () {
        return this.x;
    };
    Vierkant.prototype.berekenOppervlakte = function () {
        return this.x * this.x;
    };
    return Vierkant;
}(Figuur_1.Figuur));
exports.Vierkant = Vierkant;
