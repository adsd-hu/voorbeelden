"use strict";
// Class Figuur
exports.__esModule = true;
var Figuur = /** @class */ (function () {
    //constructor 
    function Figuur(x, y) {
        // attributes 
        this.PI = Math.PI;
        this.x = x;
        this.y = y;
    }
    // functions
    Figuur.prototype.setX = function (x) {
        this.x = x;
    };
    Figuur.prototype.setY = function (y) {
        this.y = y;
    };
    Figuur.prototype.setOmschrijving = function (omschrijving) {
        this.omschrijving = omschrijving;
    };
    Figuur.prototype.getOmschrijving = function () {
        return this.omschrijving;
    };
    return Figuur;
}());
exports.Figuur = Figuur;
