"use strict";
exports.__esModule = true;
// Class FigurenLijst
var FigurenLijst = /** @class */ (function () {
    function FigurenLijst() {
        this.items = [];
    }
    FigurenLijst.prototype.addFiguur = function (value) {
        this.items.push(value);
    };
    FigurenLijst.prototype.removeFiguur = function (value) {
        // ..
    };
    FigurenLijst.prototype.getFiguur = function (index) {
        return this.items[index];
    };
    FigurenLijst.prototype.clearAll = function () {
        // ..
    };
    FigurenLijst.prototype.sizeOff = function () {
        return this.items.length;
    };
    return FigurenLijst;
}());
exports.FigurenLijst = FigurenLijst;
