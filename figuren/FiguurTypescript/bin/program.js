"use strict";
exports.__esModule = true;
var FigurenLijst_1 = require("./FigurenLijst");
var Vierkant_1 = require("./Vierkant");
var Cilinder_1 = require("./Cilinder");
// Declare
var figurenLijst = new FigurenLijst_1.FigurenLijst();
var figuur1 = new Vierkant_1.Vierkant(5);
var figuur2 = new Cilinder_1.Cilinder(10, 3);
// Cerate objects
figuur1.setOmschrijving("Vierkant 1");
figuur2.setOmschrijving("Cilinder 1");
// Add objects to the list
figurenLijst.addFiguur(figuur1);
figurenLijst.addFiguur(figuur2);
// Iterator through list
for (var index = 0; index < figurenLijst.sizeOff(); index++) {
    var figuur;
    figuur = figurenLijst.getFiguur(index);
    console.log("Figuur omschrijving: " + figuur.getOmschrijving());
    console.log("Figuur oppervlakte:  " + figuur.berekenOppervlakte() + "\n");
}
