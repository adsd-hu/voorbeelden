"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
// Class Cilinder
var Figuur_1 = require("./Figuur");
var Cilinder = /** @class */ (function (_super) {
    __extends(Cilinder, _super);
    function Cilinder(h, r) {
        return _super.call(this, h, r) || this;
    }
    // functions
    Cilinder.prototype.getH = function () {
        return this.x;
    };
    Cilinder.prototype.getR = function () {
        return this.y;
    };
    Cilinder.prototype.berekenOppervlakte = function () {
        return (2 * this.PI * this.x * this.x) + (2 * this.PI * this.x * this.y);
    };
    return Cilinder;
}(Figuur_1.Figuur));
exports.Cilinder = Cilinder;
