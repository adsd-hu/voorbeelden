﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Figuren
{
	// Create a type-safe list class
    class FigurenLijst
    {		
		// Declare private collection
		private List<Figuur> collection;
		
		
		// Constructor
		public FigurenLijst()
		{
			collection = new List<Figuur>();
		}			
		
        // Add Figuur object
        public void AddFiguur(Figuur figuur)
        {
            collection.Add(figuur);
        }
		
		// Remove specified Figuur object from collection
		public bool RemoveFiguur(Figuur figuur)
		{
			return collection.Remove(figuur);	
		}
		
		// Verify value index is in range of collection
		// In range : return Figuur at specified index value
		// Out range: return null
		public Figuur GetFiguur(int index)
		{
			if(index >= 0 && index < collection.Count)
			{
				return collection[index];
			}
			
			return null;
		}
		
		// Clear collection
		public void ClearAll()
		{
			// Call Clear() method collection
			collection.Clear();
		}
		
		// Return number of objects stored in collection
		public int SizeOf()
		{
			// Return property Count of collection
			return collection.Count;
		}

    }
}
