using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Figuren
{
    class Vierkant : Figuur
    {
		public Vierkant()
           : base(0, 0)
        {
            // Code implementation in default constructor base class
        }
		
        // Default constructor
        public Vierkant(double x)
            : base(x, 0)
        {
            // Code implementation in default constructor base class
        }

        // Implemented polymorph method from base class
        public override double BerekenOppervlakte()
        {
            return x * x;
        }

        // Custom read-only property for X
        public double X
        {
            get
            {
                return x;
            }
			set
			{
				x = value;	
			}
        }
    }
}
