using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Figuren
{
    class Cilinder : Figuur
    {
		public Cilinder()
			: base(0, 0)
		{
			// Code implementation in default constructor base class
		}
		
        public Cilinder(double h, double r)
            : base(h, r)
        {
            // Code implementation in default constructor base class
        }

        // Implemented polymorph method from base class
        public override double BerekenOppervlakte()
        {
            return (2 * PI * x * x) + (2 * PI * x * y);
        }

        // Properties
        public double H
        {
            get
            {
                return x;
            }
			set
			{
				x = value;
			}
        }

        public double R
        {
            get
            {
                return y;
            }
			set
			{
				y = value;	
			}
        }

    }
}
