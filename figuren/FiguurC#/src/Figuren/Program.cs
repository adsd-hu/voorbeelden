/*
 * Example code implementation of Figuur with:
 * -General coding and comments (working in progress)
 * -Abstraction
 * -Encapsulation
 * -Inheritance
 * -Polymorphism by using an abstract class
 * -Constructor overloading
 * -Method/function overriding
 * -Type safe collections
 *
 * Implemented in C#.net
 *
 * See doc folder for the class diagram
 *
 * by G. Veldman
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Figuren
{
    class Program
    {
        static void Main(string[] args)
        {
			Console.WriteLine("C# Programming! \n");
			
            FigurenLijst figurenLijst = new FigurenLijst();

            Vierkant figuur1 = new Vierkant();
			figuur1.X = 5;
            figuur1.Omschrijving = "Vierkant";
            figurenLijst.AddFiguur(figuur1);

            Cilinder figuur2 = new Cilinder(10,3);
            figuur2.Omschrijving = "Cilinder";
            figurenLijst.AddFiguur(figuur2);

            Console.WriteLine("Figuur 1, {0}: {1} bij {1}", 
                                figuur1.Omschrijving, 
                                figuur1.X);
            Console.WriteLine("Figuur 2, {0}: {1},{2}", 
                                figuur2.Omschrijving, 
                                figuur2.H, 
                                figuur2.R);
			
            // Iterate figurenLijst
            // Gebruik generic type Figuur and polymorph methods
            for(int index=0; index < figurenLijst.SizeOf(); index++)
            {   
				Figuur huidigFiguur = figurenLijst.GetFiguur(index);
                Console.WriteLine("Oppervlakte {0} is: {1}", 
                                   huidigFiguur.Omschrijving,
                                   Math.Round(huidigFiguur.BerekenOppervlakte(), 2, MidpointRounding.AwayFromZero));
            }
        }
    }

	
}
