﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Figuren
{
    abstract class Figuur
    {
        // Constants
        public const double PI = Math.PI;

        // Instance variables or fields
        protected double x;
        protected double y;
        private string omschrijving;


        // Default Constructor
        public Figuur(double x, double y)
        {
            this.x = x;
            this.y = y;
        }
		

        // Property for description
        public string Omschrijving
        {
            get
            {
                return omschrijving;
            }
            set
            {
                omschrijving = value;
            }
        }
		
        // Declare polymorph method to be implemented by derived classes
        public abstract double BerekenOppervlakte();		
    }
	
}
