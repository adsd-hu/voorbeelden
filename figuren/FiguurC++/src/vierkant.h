#include <iostream>
#include "figuur.h"

using namespace std;

class CVierkant : public CFiguur 
{
public:
	// Constructor overloading linked to super class Constructor
	CVierkant()
		:CFiguur(0, 0) { };
	CVierkant(double x)
		:CFiguur(x, 0) { };
	~CVierkant() { }

	// Functions/methods		
	double berekenOppervlakte();
	double getX();
};
