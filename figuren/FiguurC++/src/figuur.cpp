#include "figuur.h"

// Constructor
CFiguur::CFiguur(double x, double y)
{
	this->x = x;
	this->y = y;
}

// set X Figuur
void CFiguur::setX(double x)
{
	this->x = x;
}

// set Y Figuur
void CFiguur::setY(double y)
{
	this->y = x;
}

// set omschrijving Figuur
void CFiguur::setOmschrijving(string omschrijving)
{
	this->omschrijving = omschrijving;
}

// get Omschrijving Figuur
string CFiguur::getOmschrijving()
{
	return this->omschrijving;
}