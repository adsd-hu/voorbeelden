#include "figurenlijst.h"

// Add specified Student pointer
void CFigurenLijst::addFiguur(CFiguur *object)
{
    collection.push_back(object);
}


// Add specified Student pointer
bool CFigurenLijst::removeFiguur(CFiguur *object)
{
	bool result = false;

	// Free List
	for(int index=0; index < collection.size(); index++)
	{
        // Free each object
        if(collection[index] == object)
        {
            // Delete memory to what te pointer is pointing to
            delete object;
	        // iteration reference (is pointer) + offset
        	collection.erase(collection.begin() + index); // Mem leak! First del mem using pointer to release memory, the remove pointer from list
        	result = true;
        }
  	}

	return result;
}


// Get specified Student pointer
CFiguur* CFigurenLijst::getFiguur(int index)
{
    // Verification of range index
    if((index >= 0) && (index < collection.size()))
    {
        return collection[index];
    }

    return 0;
}


// Free list
void CFigurenLijst::clearAll()
{
    // First delete all memory where pointerObject is pointing to
    for (vector<CFiguur*>::iterator pointerObject = collection.begin(); pointerObject != collection.end(); ++pointerObject) 
    {
        delete *pointerObject;
    }

    // Clear list
	collection.clear();
}


// Return size of list
long CFigurenLijst::sizeOf()
{
    return collection.size();
}
