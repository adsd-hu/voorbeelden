#include "figuur.h"
#include <vector>


class CFigurenLijst
{
private:
    vector<CFiguur*> collection;

public:
    void addFiguur(CFiguur *object);
    bool removeFiguur(CFiguur *object);
    CFiguur* getFiguur(int index);
    void clearAll();
    long sizeOf();
};
