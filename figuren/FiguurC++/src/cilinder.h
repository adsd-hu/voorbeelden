#include <iostream>
#include "figuur.h"

using namespace std;

class CCilinder : public CFiguur 
{
public:
	// Constructor overloading linked to super class Constructor
	CCilinder()
		:CFiguur(0, 0) { };
	CCilinder(double h, double r)
		:CFiguur(h, r) { };
	
	// Functions/methods	
	double berekenOppervlakte();
	double getH();
	double getR();
};
