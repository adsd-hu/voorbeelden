#include "vierkant.h"

// Implemented polymorph method from base class
double CVierkant::berekenOppervlakte()
{
	return x * x;
}

double CVierkant::getX()
{
	return x;
}