/*
 * Example code implementation of Figuur with:
 * -General coding and comments (working in progress)
 * -Abstraction
 * -Encapsulation
 * -Inheritance
 * -Polymorphism by using an abstract class
 * -Constructor overloading
 * -Method/function overriding
 * -Type safe collections
 *
 * Implemented in C++ (11) with header files (.h) and implementation file (.cpp)
 *
 * See doc folder for the class diagram
 *
 * by G. Veldman
 */

#include <iostream>
#include "figuur.h"
#include "vierkant.h"
#include "cilinder.h"
#include "figurenlijst.h"

using namespace std;


int main (int argc, char *argv[])
{
	cout << "C++ Programming! \n" << endl;
	
	CFigurenLijst *figurenLijst = new CFigurenLijst();
	
	// Create object instance from class by dynamic memory allocation
	CVierkant *figuur1 = new CVierkant();
	figuur1->setX(5);
	figuur1->setOmschrijving("Vierkant");
	// --> Add to figurenLijst collection
	figurenLijst->addFiguur(figuur1);
	cout << "Figuur 1, " << figuur1->getOmschrijving() << ": " << figuur1->getX() << " bij " << figuur1->getX() << endl;
	
	CCilinder *figuur2 = new CCilinder(10,3);
	figuur2->setOmschrijving("Cilinder");
	// --> Add to figurenLijst collection
	figurenLijst->addFiguur(figuur2);
	cout << "Figuur 2, " << figuur2->getOmschrijving() << ": " << figuur2->getH() << ", " << figuur2->getR() << endl;
		
	// Iterate figurenLijst
    // Gebruik generic type Figuur and polymorph methods
	for(int index=0; index < figurenLijst->sizeOf(); index++)
	{
        CFiguur* currentFiguur = figurenLijst->getFiguur(index);

		cout << "Oppervlakte " << currentFiguur->getOmschrijving() << " is: ";
		// round value
		printf("%.2lf", currentFiguur->berekenOppervlakte());
		cout << endl;
	}
	
	
	// --X Free dynamic memory allocation (by keyword new)
	//figurenLijst->clearAll();
	delete figurenLijst;
	
	return 0;	
}
