#include "cilinder.h"

// Implemented polymorph method from base class
double CCilinder::berekenOppervlakte()
{
	return (2 * PI * x * x) + (2 * PI * x * y);
}

double CCilinder::getH()
{
	return x;
}

double CCilinder::getR()
{
	return y;
}