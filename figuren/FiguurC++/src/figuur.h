#pragma once
#include <iostream>

using namespace std;

// Abstract class Figuur
class CFiguur {
protected:
    double x;
    double y;
private:
    string omschrijving;
public:
	const double PI = 3.14159265;
	CFiguur(double x, double y);
	virtual ~CFiguur() { };
	
	void setX(double x);
	void setY(double y);
	void setOmschrijving(string omschrijving);
	string getOmschrijving();
	
	virtual double berekenOppervlakte() =0;
};