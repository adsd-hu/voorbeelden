package figuren;

/*
 * In this class some test cases are defined based on JUnit
 */

import org.junit.Test;
import static org.junit.Assert.assertEquals;


public class Tester
{
    // Define some unittests on the level of method

    @Test
    // Testcase 1
    // Test berekenOppervlakte()
    // Result should be 36 -- simulate failure
    public void testCase1() 
    {
        Vierkant figuur = new Vierkant(6);
        assertEquals(36.0, figuur.berekenOppervlakte(), 0);
    }  


    @Test
    // Testcase 2
    // Test berekenOppervlakte()
    // Result should be 25
    public void testCase2() 
    {
        Vierkant figuur = new Vierkant(5);
        assertEquals(25.0, figuur.berekenOppervlakte(), 0);
    }


    @Test
    // Testcase 3
    // Test berekenOppervlakte()
    // Result should be "Vierkant"
    public void testCase3() 
    {
        Vierkant figuur = new Vierkant();
        figuur.setOmschrijving("Vierkant"); 
        assertEquals("Vierkant", figuur.getOmschrijving());
    }    


    @Test
    // Testcase 4
    // Test berekenOppervlakte()
    // Result should be 816.8140899333462
    public void testCase4() 
    {
        Cilinder figuur = new Cilinder(10,3);
        assertEquals(816.8140899333462, figuur.berekenOppervlakte(), 0);
    }   


    @Test
    // Testcase 5
    // Test berekenOppervlakte()
    // Result should be 5969.026041820607
    public void testCase5() 
    {
        Cilinder figuur = new Cilinder(25,13);
        assertEquals(5969.026041820607, figuur.berekenOppervlakte(), 0);
    }


    @Test
    // Testcase 6
    // Test berekenOppervlakte()
    // Result should be "Cilinder"
    public void testCase6() 
    {
        Cilinder figuur = new Cilinder();
        figuur.setOmschrijving("Cilinder");
        assertEquals("Cilinder", figuur.getOmschrijving());
    }  

}
