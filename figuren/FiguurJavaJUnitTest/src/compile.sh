clear
echo Compiling..
echo
rm -f ../bin/figuren/*.class
javac -Xlint -cp .:../ext/junit-platform-console-standalone-1.5.2.jar ./figuren/Program.java
mv ./figuren/*.class ../bin/figuren
echo Done!
echo
ls -l ../bin/figuren/*.class
