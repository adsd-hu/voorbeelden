
  Example code implementation of Figuur with:
  -General coding and comments (working in progress)
  -Abstraction
  -Encapsulation
  -Inheritance
  -Polymorphism by using an abstract class
  -Constructor overloading
  -Method/function overriding
  -Type safe collections
 
  Implemented in:
  C#
  C++
  Java
  Java with JUnit test
  Java with Singleton FigurenLijst
  Java with Test
  Php
  Python (dynamic typed)
  Typescript (static typed)
  
  See doc folder for the class diagram
 