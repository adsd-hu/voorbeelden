class FigurenLijst:
	# Create a Python3 dynamic list
	collection = []

	# Constructor of this class
	def __init__(self):
		pass

	# Class fucntions
	def addFiguur(self, figuur):
		self.collection.append(figuur)

	# Returns the specified Figuur object
	def getFiguur(self, index):
		if(index < len(self.collection)):
			return self.collection[index]
		else:
			return 0 # or None

	def sizeOf(self):
		return len(self.collection)

	# Returns the collection for iteration
	def getCollection(self):
		return self.collection;
