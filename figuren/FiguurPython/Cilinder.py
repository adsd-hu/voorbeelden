from Figuur import Figuur

# Inhertence class Figuur
class Cilinder(Figuur):

    # Constructor of this class
    def __init__(self, h=None, r=None):
        # Implement Constructor overloading Cilinder() and Cilinder(h, r)
        if h is None and r is None:
            self._x = 0
            self._y = 0
        else:
            self._x = h
            self._y = r
    
    # Class methods
    def getH(self):
        return self._x

    def getR(self):
        return self._y   

    def berekenOppervlakte(self):
        return (2 * self.PI * self._x * self._x) + (2 * self.PI * self._x * self._y)
