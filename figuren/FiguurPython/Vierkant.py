from Figuur import Figuur

# Inhertence class Figuur
class Vierkant(Figuur):

    # Constructor of this class
    def __init__(self, x=None):
        # Implement Constructor overloading Vierkant() and Vierkant(x)
        if x is None:
            self._x = 0
        else:
            self._x = x
    
    # Class methods
    def getX(self):
        return self._x

    def berekenOppervlakte(self):
        return self._x * self._x