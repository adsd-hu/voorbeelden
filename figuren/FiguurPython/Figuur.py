# Class Figuur, abstract by use of pass
import math

class Figuur:
    PI = math.pi; # declared as public without underscore
    _x = 0 # declared as protected _
    _y = 0 # declared as protected _
    __omschrijving = "" # declared as private __

    # Constructor of this class
    def __init__(self, x, y):
        self._x = x
        self._y = y
    
    # Class methods
    def setX(self, x):
        self._x = x

    def setY(self, y):
        self._y = y

    def setOmschrijving(self, omschrijving):
        self.__omschrijving = omschrijving

    def getOmschrijving(self):
        return self.__omschrijving

    # Abstract function
    def berekenOppervlakte(self):
        pass