 #
 # Example code implementation of Figuur with:
 # -General coding and comments (working in progress)
 # -Abstraction
 # -Encapsulation
 # -Inheritance
 # -Polymorphism by using an abstract class
 # -Constructor overloading
 # -Method/function overriding
 # -Type safe collections
 #
 # Implemented in Python 3
 #
 # See doc folder for the class diagram
 #
 # by G. Veldman
 #

# Import local files
from FigurenLijst import FigurenLijst
from Cilinder import Cilinder
from Vierkant import Vierkant

class Program:

    def run(self):
        # Create objects
        figurenLijst = FigurenLijst()

        figuur1 = Vierkant()
        figuur1.setX(5)
        figuur1.setOmschrijving("Vierkant")
        figurenLijst.addFiguur(figuur1)

        figuur2 = Cilinder(10,3);
        figuur3 = Cilinder(25,13);
        figuur2.setOmschrijving("Cilinder 1");
        figuur3.setOmschrijving("Cilinder 2");

        figurenLijst.addFiguur(figuur2)
        figurenLijst.addFiguur(figuur3)


        ## Iteration through list

        # Get the iterable collection
        for figuur in figurenLijst.getCollection():
        	# bereken oppervlakte
        	oppervlakte = figuur.berekenOppervlakte()
        	# print oppervlakte
        	print("Oppervlakt " + figuur.getOmschrijving() + " is: " + str( round(oppervlakte, 2) )  )


        print("\nSame output by traditional for loop\n")

        # Use a traditional for loop
        for index in range(figurenLijst.sizeOf()):
        	figuur = figurenLijst.getFiguur(index)
        	# bereken oppervlakte
        	oppervlakte = figuur.berekenOppervlakte()
        	# print oppervlakte
        	print("Oppervlakt " + figuur.getOmschrijving() + " is: " + str( round(oppervlakte, 2) )  )
# END of class


# Entry or start point of the app
app = Program()
app.run()