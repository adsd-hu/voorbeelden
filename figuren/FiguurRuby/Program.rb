#!/usr/bin/ruby -w

#
# Example code implementation of Figuur with:
# -General coding and comments (working in progress)
# -Abstraction
# -Encapsulation
# -Inheritance
# -Polymorphism by using an abstract class
# -Constructor overloading
# -Method/function overriding
# -Type safe collections
#
# Implemented in Kotlin on macOS
#
# See doc folder for the class diagram
#
# by G. Veldman
#/

# import required classes
require_relative "FigurenLijst.rb"
require_relative "Vierkant.rb"
require_relative "Cilinder.rb"

class Program

	public
		def run()
			# Create objects
			figurenLijst = FigurenLijst.new()

			figuur1 = Vierkant.new()
			figuur1.setX(5)
			figuur1.setOmschrijving("Vierkant")
			figurenLijst.addFiguur(figuur1)

			figuur2 = Cilinder.new(10, 3)
			figuur3 = Cilinder.new(25, 13)
			figuur2.setOmschrijving("Cilinder 1")
			figuur3.setOmschrijving("Cilinder 2")

			figurenLijst.addFiguur(figuur2)
			figurenLijst.addFiguur(figuur3)								

			puts "*Figuren demo implemented in Ruby *"
			puts
			puts "Figuur 1, #{figuur1.getOmschrijving()} : #{figuur1.getX()} bij #{figuur1.getX()}"
			puts "Figuur 2, #{figuur2.getOmschrijving()} : #{figuur2.getH()} bij #{figuur2.getR()}"
			
			# Iterate through the collection figurenLijst
			for indexl in 0..(figurenLijst.sizeOf()-1)
				# Get current figuur from the collection figurenLijst
				huidigeFiguur = figurenLijst.getFiguur(indexl)

			    puts "Oppervlakte #{huidigeFiguur.getOmschrijving()} is #{(huidigeFiguur.berekenOppervlakte()).round(2)}"
			end

		end
end

# Entry or start point of the application
app = Program.new
app.run()