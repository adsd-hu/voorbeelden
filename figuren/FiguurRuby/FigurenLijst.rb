# Class FigurenLijst, a specific collection of objects
require 'set'

class FigurenLijst
   private
      # initialize attributes
      @collection = 0

   public

      # Make this class abstract and not implementable
      # constructor method
      def initialize()
         @collection = Set.new()
      end 


      # Set x and y
      def addFiguur(figuur)
         @collection.add(figuur)
      end

      # Return object Figuur at location specified bij index
      # Return NIL if not exists
      def getFiguur(index)
         return @collection.to_a[index]
      end   

      # Return sizeOf the collection
      def sizeOf()
         @collection.size()
      end

      # Clear the collection
      def clearAll()
         @collection.delete(remove_array)
      end

end