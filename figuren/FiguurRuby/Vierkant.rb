require_relative "Figuur.rb"

class Vierkant < Figuur

   public
      # constructor method
      # constructor overloading does not exist
      # Create Vierkant() and Vierkant(x) in this way      
      def initialize(*x)
         if x.size == 0 # Vierkant()
            @x = 0         
         elsif x.size == 1 # Vierkant(x)
            @x = x
         else
            raise "NotASupportedConstructor"
         end         
      end

      # Return x
      def getX()
         return @x
      end

      # Implemented method from abstract class
      def berekenOppervlakte()
         return @x * @x
      end
end