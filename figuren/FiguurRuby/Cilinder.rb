require_relative "Figuur.rb"

class Cilinder < Figuur

   public
      # constructor method
      # constructor overloading does not exist
      # Create Cilinder() and Cilinder(h, r) in this way
      def initialize(*hr)
         if hr.size == 0 # Cilinder()
            @x = 0
            @y = 0         
         elsif hr.size == 2 # Cilinder(h, r)
            @x = hr[0] # hooghte h
            @y = hr[1] # straal r
         else
            raise "NotASupportedConstructor"
         end
      end

      # Return H
      def getH()
         return @x
      end

      # Return R
      def getR()
         return @y
      end

      # Implemented method from abstract class
      def berekenOppervlakte()
         return (2 * PI * @x * @x) + (2 * PI * @x * @y)
      end
end