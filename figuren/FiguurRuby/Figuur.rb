# Abstract class Figuur

class Figuur
   protected
      # initialize attributes
      @x = 0
      @y = 0
      @omschrijving = ""

   public
      PI = Math::PI #3.14

      # Make this class abstract and not implementable
      # constructor method
      def initialize()
         raise "Cannot initialize an abstract Figuur class"
      end 


      # Set x and y
      def setX(x)
         @x = x
      end

      def setY(y)
         @y = y
      end   

      # Set/get omschrijving
      def setOmschrijving(omschrijving)
         @omschrijving = omschrijving
      end

      def getOmschrijving()
         return @omschrijving
      end


      # Abstract methods berekenOppervlakte
      # Make this method abstract without implementation
      # Implementation made by sub class
      def berekenOppervlakte()
         raise NotImplementedError # because this method is abstract
      end      

end