/*
 * Example code implementation of Figuur with:
 * -General coding and comments (working in progress)
 * -Abstraction
 * -Encapsulation
 * -Inheritance
 * -Polymorphism by using an abstract class
 * -Constructor overloading
 * -Method/function overriding
 * -Type safe collections
 *
 * Implemented in Java with the Singleton pattern
 *
 * See doc folder for the class diagram
 *
 * by G. Veldman
 */
package figuren;

import figuren.Figuur;

public class Program 
{
	public static void main(String[] args) 
	{
		System.out.println( "Java Programming! \n");
		
        // Get Singleton instance for Figuurlijst
        FigurenLijst figurenLijst = FigurenLijst.getInstance();

        Figuur figuur1 = new FiguurFactory().getFiguur("Vierkant");
        figuur1.setOmschrijving("Vierkant");
        figurenLijst.addFiguur(figuur1);

        Figuur figuur2 = new FiguurFactory().getFiguur("Cilinder");
        Figuur figuur3 = new FiguurFactory().getFiguur("Cilinder");
        figuur2.setOmschrijving("Cilinder 1");
        figuur3.setOmschrijving("Cilinder 2");
        
        figurenLijst.addFiguur(figuur2);
        figurenLijst.addFiguur(figuur3);
        
        // Iterate figurenLijst
        // Gebruik generic type Figuur and polymorph methods
        for(int index=0; index < figurenLijst.sizeOf(); index++)
        {   
            Figuur huidigFiguur = figurenLijst.getFiguur(index);
            
            System.out.printf("Oppervlakte %s is: %.2f \n", 
                               huidigFiguur.getOmschrijving(),
                               huidigFiguur.berekenOppervlakte());
        }
	}

}
