package figuren;

public class FiguurFactory {
    
    public Figuur getFiguur(String figuurType) {

        if (figuurType.equalsIgnoreCase("CILINDER")) {
            return new Cilinder();
        } else if (figuurType.equalsIgnoreCase("VIERKANT")) {
            return new Vierkant();
        }
        
        return null;
    }
}