package figuren;

import figuren.Figuur;
import java.util.*;

// Create an small interface safe and type-safe list class
public class FigurenLijst
{
	// Declare a private collection
	private Vector<Figuur> collection;
	
	
	public FigurenLijst()
	{
		collection = new Vector<Figuur>();
	}
	
	
	// Add Figuur object
	public boolean addFiguur(Figuur figuur)
	{
		return collection.add(figuur);
	}
	
	// Remove specified Figuur from the collection
	public boolean removeFiguur(Figuur figuur)
	{
		return collection.remove(figuur);
	}
	
	// Return the Figuur object specified by index
	public Figuur getFiguur(int index)
	{
		return collection.elementAt(index);
	}

	// Clear the collection
	public void clearAll()
	{
		collection.clear();
	}
	
	// Returns the current size of the collection
	public int sizeOf()
	{
		return collection.size();
	}

}
