clear
echo Compiling..
echo
rm -f ../bin/figuren/*.class
javac -Xlint ./figuren/Program.java
mv ./figuren/*.class ../bin/figuren
echo Done!
echo
ls -l ../bin/figuren/*.class
