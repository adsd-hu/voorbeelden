package figuren;

public abstract class Figuur 
{
    // Constants
    public final double PI = Math.PI;

    // Instance variables or fields
    protected double x;
    protected double y;
    private String omschrijving;

 
    // Default Constructor
    public Figuur(double x, double y)
    {
        this.x = x;
        this.y = y;
    }
    
    
    public void setX(double x)
    {
    	this.x = x;
    }
   
    public void setY(double y)
    {
    	this.y = y;
    }
    
    public void setOmschrijving(String omschrijving)
    {
    	this.omschrijving = omschrijving;
    }
    
    public String getOmschrijving()
    {
    	return this.omschrijving;
    }
    
    // Declare polymorph method to be implemented by derived classes
    public abstract double berekenOppervlakte();

}
