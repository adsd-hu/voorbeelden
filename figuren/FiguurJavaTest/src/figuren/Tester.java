package figuren;

/*
 * In this class some test cases are defined
 */

public class Tester
{
    private String testHeader = new String("#Test result @testCase ");
    private Boolean overallTestResult = true;


    // Run the tester
    public Boolean runTester()
    {
        System.out.println("Run tester..");

        // Execute and display results of the defined unittests

        // Testcase 1, step-by-step written
        int testCase = 1;
        Boolean testResult = testCase1();
        printTestResult(testCase, testResult);

        // Testcase 2, short written
        printTestResult(2, testCase2()); 

         // Testcase 3              
        printTestResult(3, testCase3());

         // Testcase 4       
        printTestResult(4, testCase4());

        // Testcase 5        
        printTestResult(5, testCase5());

        // Testcase 6        
        printTestResult(6, testCase6());


        return overallTestResult;
    }



    //=== START Define some unit test on level of methods

    // Test berekenOppervlakte()
    // Result should be 25.0
    private Boolean testCase1()
    {
        // Step-by-step written
        //
        Vierkant figuur = new Vierkant(5);
        // Make a Double object to compare its value later
        Double oppervlakte = Double.valueOf( figuur.berekenOppervlakte() );
        // Execute test and save the test result
        Boolean testResult = oppervlakte.equals(25.0);
        
        // Evaluate the result on the overall test result
        testResult = setOverallTestResult(testResult);

        // Return the result of this testcase
        return testResult;
    }


    // Test berekenOppervlakte()
    // Result should be 225.0
    private Boolean testCase2()
    {
        // Short written
        //
        Vierkant figuur = new Vierkant(15);

        return setOverallTestResult( Double.valueOf( figuur.berekenOppervlakte() ).equals(225.0) );
    }


    // Test setOmschrijving()/getOmschrijving()
    // Result should be "Vierkant"
    private Boolean testCase3()
    {
        Vierkant figuur = new Vierkant();
        figuur.setOmschrijving("Vierkant"); 

        return setOverallTestResult(String.valueOf( figuur.getOmschrijving() ).equals("Vierkant") );        
    }


    // Test berekenOppervlakte()
    // Result figuur 3 should be 816.8140899333462
    private Boolean testCase4()
    {
        Cilinder figuur = new Cilinder(10,3);

        return setOverallTestResult(Double.valueOf( figuur.berekenOppervlakte() ).equals(816.8140899333462) );        
    }


    // Test berekenOppervlakte()
    // Result should be 5969.026041820607
    private Boolean testCase5()
    {
        Cilinder figuur = new Cilinder(25,13);

        return setOverallTestResult(Double.valueOf( figuur.berekenOppervlakte() ).equals(5969.026041820607) );        
    }


    // Test setOmschrijving()/getOmschrijving()
    // Result figuur 3 should be "Cilinder"
    private Boolean testCase6()
    {
        Cilinder figuur = new Cilinder();
        figuur.setOmschrijving("Cilinder");

        return setOverallTestResult(String.valueOf( figuur.getOmschrijving() ).equals("Cilinder") );   
    }            

    //=== END




    // Set overallTestResult to false when a testCase fails
    private Boolean setOverallTestResult(Boolean testResult)
    {
        if(!testResult)
        {
            overallTestResult = false;
        }

        return testResult;
    }


    // Print the test results
    private void printTestResult(int testCase, Boolean testResult)
    {
        System.out.println("\t" + testHeader + String.valueOf(testCase) + ": " + testResult);
    }

}
