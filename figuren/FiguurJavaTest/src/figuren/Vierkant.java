package figuren;

import figuren.Figuur;

public class Vierkant extends Figuur 
{
    // Default constructor
    public Vierkant()
    {
        // Code implementation in default constructor base class
    	super(0, 0);
    }
    
    // Default constructor
    public Vierkant(double x)
    {
        // Code implementation in default constructor base class
    	super(x, 0);
    }

    // Implemented polymorph method from base class
    public double berekenOppervlakte()
    {
        return x * x;
    }

    // Custom read-only property for X
    public double getX()
    {
    	return x;
    }
}
