
// Create a claas with an empty constructor ()
public class Cilinder() : Figuur()
{
	// Constructor overloading (2nd constructor)
	constructor(h: Double, r: Double): this()
	{
		x = h;
		y = r;
	}

	// Use _ between set/get and variable to prevent clash with auto set/get
	public fun get_H() : Double
	{
		return super.x
	}

	public fun get_R() : Double
	{
		return super.y
	}	

	public override fun berekenOppervlakte() : Double
	{
		return (2 * PI * x * x) + (2 * PI * x * y);
	}
}