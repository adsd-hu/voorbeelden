
// Create a class with an empty constructor ()
public abstract class Figuur() 
{
	// Constants
	public val PI = kotlin.math.PI;

	// Instance variables or fields
	protected var x: Double = 0.0
	protected var y: Double = 0.0
	private var omschrijving: String = ""

	// Constructor overloading (2nd constructor)
	constructor(x: Double, y: Double) : this()
	{
		this.x = x
		this.y = y	
	}

	// Use _ between set/get and variable to prevent clash with auto set/get

	public fun set_X(x: Double)
	{
		this.x = x
	}

	public fun set_Y(y: Double)
	{
		this.y = y
	}

	public fun set_Omschrijving(omschrijving: String)
	{
		this.omschrijving = omschrijving
	}

	public fun get_Omschrijving(): String
	{
		return this.omschrijving
	}

	public abstract fun berekenOppervlakte() : Double
   
	// member function
	public fun printFigure() 
	{
		print("Creation of " + this.omschrijving + "\n")
	}
}