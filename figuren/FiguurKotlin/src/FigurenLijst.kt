public class FigurenLijst
{
	private var collection: ArrayList<Figuur>

	constructor()
	{
		collection = ArrayList<Figuur>()
	}

	// Add Figuur object
	public fun addFiguur(figuur: Figuur): Boolean
	{
		return collection.add(figuur)
	}

	// Return the Figuur object specified by index
	public fun getFiguur(index: Int): Figuur
	{
		return collection.get(index)
	}

	// Clear the collection
	public fun clearAll()
	{
		collection.clear()
	}

	// Returns the current size of the collection
	public fun sizeOf(): Int
	{
		return collection.size
	}

}