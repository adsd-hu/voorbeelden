
// Create a claas with an empty constructor ()
public class Vierkant() : Figuur()
{
	// Constructor overloading (2nd constructor)
	constructor(x: Double) : this()
	{
		super.x = x
	}

	// Use _ between set/get and variable to prevent clash with auto set/get
	public fun get_X() : Double
	{
		return super.x
	}

	public override fun berekenOppervlakte() : Double
	{
		return x * x;
	}
}