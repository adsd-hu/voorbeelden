/*
 * Example code implementation of Figuur with:
 * -General coding and comments (working in progress)
 * -Abstraction
 * -Encapsulation
 * -Inheritance
 * -Polymorphism by using an abstract class
 * -Constructor overloading
 * -Method/function overriding
 * -Type safe collections
 *
 * Implemented in Kotlin on macOS
 *
 * See doc folder for the class diagram
 *
 * by G. Veldman
 */
public class Program()
{
	public fun run()
	{
		val figurenLijst = FigurenLijst()

	    val figuur1: Vierkant = Vierkant()
	    figuur1.set_X(5.0)
	   	figuur1.set_Omschrijving("Vierkant 1")
	    figurenLijst.addFiguur(figuur1)

	    val figuur2: Cilinder = Cilinder(10.0, 3.0)
	    val figuur3: Cilinder = Cilinder(25.0, 13.0)
	    figuur2.set_Omschrijving("Cilinder 1")
	    figuur3.set_Omschrijving("Cilinder 2")
	    figurenLijst.addFiguur(figuur2)
	    figurenLijst.addFiguur(figuur3)

	    println("*Figuren demo implemented in Kotlin, executed on Java Runtime Environment*\n")
	    println("Figuur 1, " + figuur1.get_Omschrijving() + 
	    		": " + figuur1.get_X() + " bij " + figuur1.get_X())	    
	    println("Figuur 2, " + figuur2.get_Omschrijving() + 
	    		": " + figuur2.get_H() + " bij " + figuur2.get_R())

        // Iterate figurenLijst
        // Gebruik generic type Figuur and polymorph methods
        // The collection array starts with index 0 with end as size - 1
	    for(index in 0..figurenLijst.sizeOf() - 1)
	    {
	    	var huidigeFiguur: Figuur = figurenLijst.getFiguur(index)
		    var oppervlakt = String.format("%.2f", huidigeFiguur.berekenOppervlakte() )	    
		    print("Oppervlakt " + huidigeFiguur.get_Omschrijving() + 
		    				" is " + oppervlakt + "\n")	    	
	    }
	}
}

// Entry point of program
fun main() 
{
	val app = Program()
	app.run()
}
