internal protocol FiguurProtocol
{
	/* Define the abstract method in a protocol, abstract classes does not exist in Swift.
	 * Accessmodifier not allowed in a protocol.
	 * Use delegate to access this method in sub classes from super abstraction level Figuur.
	 */
	func berekenOppervlakt() -> Double
}

public class Figuur
{
	// Constants
	public let PI: Double = Double.pi

	// Instance variables or fields
	// Using accessmodifier internal because protected does not exist in Swift
	internal var delegate: FiguurProtocol!
	internal var x: Double = 0
	internal var y: Double = 0
    internal var omschrijving: String = "Figuur"

    // Constructor like elements called init
    public init()
    {
    	// Just empty
    }

    public init(x: Double, y: Double)
    {
 		self.x = x
		self.y = y
    }


    // Methods
	public func setX(x: Double)
	{
		self.x = x
	}

	public func setY(y: Double)
	{
		self.y = y
	}

	public func setOmschrijving(omschrijving: String)
	{
		self.omschrijving = omschrijving
	}

	public func getOmschrijving() -> String
	{
		return self.omschrijving
	}

	/*
	 * Because abstract methods and classes does not exist in Swift,
	 * delegate is used to have access to a specifiec implementation
	 * of the protocol method berekenOppervlakt from a super abstraction level.
	 * These method cannot have te same name to avoid overriding conflicts
	 * and because it requires an implementation to the delegate method of any
	 * sub class create from this class. For example see main.swift.
	 */
	public func callBerekenOppervlakt() -> Double
	{
		return delegate.berekenOppervlakt()
	}

	public func printFigure() 
	{
		print("Creation of " + self.omschrijving)
	}
}