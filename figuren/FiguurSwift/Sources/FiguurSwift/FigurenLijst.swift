public class FigurenLijst
{

    // Declare a typesafe Array
    private var collection: Array<Figuur>

    // Constructor like elements called init
    public init()
    {
    	collection = Array<Figuur>()
    }

    // Methods
    // Add Figuur object
    public func addFiguur(figuur: Figuur)
    {
        collection.append(figuur)
    }

    // Return the specific Figuur object specified by index
    // Returns a NullFiguur object when the specified index is out of bounds
    public func getFiguur(index: Int) -> Figuur
    {
        if index >= 0 && index < collection.count
        {
            return collection[index]
        }
        else {
            return NullFiguur()
        }
    }

    // Clear the collection
    public func clearAll()
    {
        collection.removeAll()
    }

    // Returns the current size of the collection
    public func sizeOf() -> Int
    {
        return collection.count
    }    

}