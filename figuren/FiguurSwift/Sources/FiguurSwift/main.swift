/*
 * Example code implementation of Figuur with:
 * -General coding and comments (working in progress)
 * -Abstraction
 * -Encapsulation
 * -Inheritance
 * -Polymorphism by using an abstract class
 * -Constructor overloading
 * -Method/function overriding
 * -Type safe collections
 *
 * Implemented in Swift 5 on macOS
 *
 * See doc folder for the class diagram
 *
 * by G. Veldman
 */

import Foundation

internal class Program 
{
    func run()
    {
    	let figurenLijst = FigurenLijst()

		let figuur1: Vierkant = Vierkant()
		figuur1.setX(x: 5.0)
		figuur1.setOmschrijving(omschrijving: "Vierkant 1")
		figurenLijst.addFiguur(figuur: figuur1)

	    let figuur2: Cilinder = Cilinder(h: 10.0, r: 3.0)
	    let figuur3: Cilinder = Cilinder(h: 25.0, r: 13.0)
	    figuur2.setOmschrijving(omschrijving: "Cilinder 1")
	    figuur3.setOmschrijving(omschrijving: "Cilinder 2")
	    figurenLijst.addFiguur(figuur: figuur2)
	    figurenLijst.addFiguur(figuur: figuur3)

	    print("*Figuren demo implemented in Swift 5*\n")
	    print("Figuur 1, ", figuur1.getOmschrijving(), 
	    		": ", figuur1.getX(), " bij ", figuur1.getX())	    
	    print("Figuur 2, ", figuur2.getOmschrijving(), 
	    		": ", figuur2.getH(), " bij ", figuur2.getR())

        // Iterate figurenLijst on a classic way to see what happens
        // Gebruik generic type Figuur and polymorph methods
        // The collection array starts with index 0 with end as size - 1
	    for index in 0..<figurenLijst.sizeOf()
	    {
	    	// Get object on abstract level Figuur
	    	let huidigeFiguur = figurenLijst.getFiguur(index: index)

	    	// The proper implemented protocol method is called by a delegate in the
	    	// callBerekenOppervlakt method. See class Figuur for more detail.
	    	let oppervlakt = String(format: "%.2f", huidigeFiguur.callBerekenOppervlakt())
		    print("Oppervlakt ", huidigeFiguur.getOmschrijving(), 
		    			" is ", oppervlakt)
	    }
    }
}

// Entry or start point of the App
let app = Program()
app.run()
