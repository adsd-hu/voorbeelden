//
// Inheritance of class Figuur and implement FiguurProtocol
//
public class Cilinder : Figuur, FiguurProtocol
{
	// Constructor like overloading from super class
	public override init()
	{
		super.init(x: 0, y: 0)
		delegate = self
	}

	public init(h: Double, r: Double)
	{
		// Calls the super init as a Constructor
		super.init(x: h, y: r)
		delegate = self		
	}


	// Methods
	public func getH() -> Double
	{
		return super.x
	}

	public func getR() -> Double
	{
		return super.y
	}	

	// This method is called by delegate in Figuur
    public func berekenOppervlakt() -> Double
    {
		return (2 * PI * x * x) + (2 * PI * x * y);
    }
}