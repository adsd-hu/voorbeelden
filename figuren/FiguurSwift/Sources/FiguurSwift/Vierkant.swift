//
// Inheritance of class Figuur and implement FiguurProtocol
//
public class Vierkant : Figuur, FiguurProtocol
{
	// Constructor like overloading from super class
	public override init()
	{
		super.init(x: 0, y: 0)
		delegate = self
	}

	public init(x: Double)
	{
		// Calls the super init as a Constructor
		super.init(x: x, y: 0)
		delegate = self		
	}

	// Methods
	public func getX() -> Double
	{
		return super.x
	}

	// This method is called by delegate in Figuur
    public func berekenOppervlakt() -> Double
    {
		return x * x;
    }
}