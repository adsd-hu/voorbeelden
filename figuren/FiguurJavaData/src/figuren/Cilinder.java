package figuren;

import figuren.Figuur;

public class Cilinder extends Figuur
{	
    // Constructor
    public Cilinder()
    {
        // Code implementation in default constructor base class
    	super(0, 0);
    }
    
    // Default constructor
    public Cilinder(double h, double r)
    {
        // Code implementation in default constructor base class
    	super(h, r);
    }

    // Default constructor
    public Cilinder(double h, double r, String omschrijving)
    {
        // Code implementation in default constructor base class
        super(h, r);
        super.setOmschrijving(omschrijving);
    }    

    // Implemented polymorph method from base class
    public double berekenOppervlakte()
    {
    	return (2 * PI * x * x) + (2 * PI * x * y);
    }

    // Custom read-only property for X
    public double getH()
    {
    	return x;
    }
    
    // Custom read-only property for X
    public double getR()
    {
    	return y;
    }    
}
