package figuren;

import java.lang.Integer;
import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


public class DataModel
{
	// Defined as a final. Can be made configurable by Program arguments
	private final String xmlFile = "../data/figuren.xml";


	public FigurenLijst getFigurenLijst()
	{
		// Use a XML data source
		// This can be changed in any data source, like SQL or JSON
        try 
        {
            return parseXmlData(xmlFile);
        } 
        catch (Exception e)
        {
            // An exception occured during XML parsing. Check your XML filelocation and structure.
        	// Instead of return a null, throw the proper (custom) Exception object to handle any
        	// exceptions that may occur
            return null;
        }		

	}


    private FigurenLijst parseXmlData (String xmlFile) throws
                                                         ParserConfigurationException,
                                                         SAXException,
                                                         IOException 
    {
    	FigurenLijst figurenLijst = new FigurenLijst();

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		// Load the input XML document, parse it
		Document document = builder.parse(new File(xmlFile));
		NodeList nodeList = document.getDocumentElement().getChildNodes();


		// Iterate through xml data nodes
		for (int i = 0; i < nodeList.getLength(); i++) 
		{
			Node node = nodeList.item(i);

			// Get specified data based on supported figuurType
			if (node.getNodeType() == Node.ELEMENT_NODE) 
			{
			    Element element = (Element) node;

			    // Get the value of the attributes
			    String figuurType = node.getAttributes().getNamedItem("type").getNodeValue();


			    if(figuurType.equals("Vierkant"))
			    {
			      // Properties of figuur Vierkant
			      Double x = Double.parseDouble(element.getElementsByTagName("X")
			                          .item(0).getChildNodes().item(0).getNodeValue());

			      String omschrijving = element.getElementsByTagName("Omschrijving")
			                          .item(0).getChildNodes().item(0).getNodeValue();

			      Vierkant figuur = new Vierkant(x);
			      figuur.setOmschrijving(omschrijving);     

			      figurenLijst.addFiguur(figuur);
			    }

			    else if(figuurType.equals("Cilinder"))
			    {
			      // Properties of figuur Cilinder
			      Double h = Double.parseDouble(element.getElementsByTagName("H")
			                          .item(0).getChildNodes().item(0).getNodeValue());

			      Double r = Double.parseDouble(element.getElementsByTagName("R")
			                          .item(0).getChildNodes().item(0).getNodeValue());			      

			      String omschrijving = element.getElementsByTagName("Omschrijving")
			                          .item(0).getChildNodes().item(0).getNodeValue();

			      figurenLijst.addFiguur(new Cilinder(h, r, omschrijving));
			    }
			}
		}
        
        return figurenLijst;
    }

}