/*
 * Example code implementation of Figuur with:
 * -General coding and comments (working in progress)
 * -Abstraction
 * -Encapsulation
 * -Inheritance
 * -Polymorphism by using an abstract class
 * -Constructor overloading
 * -Method/function overriding
 * -Type safe collections
 * -External data, like XML
 *
 * Implemented in Java
 *
 * See doc folder for the class diagram
 *
 * by G. Veldman
 */
package figuren;

import figuren.Figuur;

public class Program 
{
	public static void main(String[] args) 
	{
        DataModel figurenDataModel = new DataModel();

        FigurenLijst figurenLijst = figurenDataModel.getFigurenLijst();

        
        if(figurenLijst != null)
        {
            // Iterate figurenLijst
            // Gebruik generic type Figuur and polymorph methods
            for(int index=0; index < figurenLijst.sizeOf(); index++)
            {   
                Figuur huidigFiguur = figurenLijst.getFiguur(index);
                
                System.out.printf("Oppervlakte %s is: %.2f \n", 
                                   huidigFiguur.getOmschrijving(),
                                   huidigFiguur.berekenOppervlakte());
            }
        }
        else
        {
            System.err.println("\nAn exception occured during data processing from the data source.");
            System.err.println("Please check the data source location and its structure.\n");
        }
	}

}
