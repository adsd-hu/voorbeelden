package figuren;

import figuren.Figuur;
import java.util.*;

// Create a small type-safe list class using the Singleton designpattern
// Using the Singleton designpattern to ensure one object instance of FiguurLijst
public class FigurenLijst
{
	// Declare a private collection
	private Vector<Figuur> collection;
	// Create private instance for Singleton
	private static final FigurenLijst instance = new FigurenLijst();
	
	// Private constructor for Singleton
	private FigurenLijst()
	{
		collection = new Vector<Figuur>();
	}

	// public getInstance method te return the one object instance for Singelton
	public static FigurenLijst getInstance()
	{
		return instance;
	}
	
	
	// Add Figuur object
	public boolean addFiguur(Figuur figuur)
	{
		return collection.add(figuur);
	}
	
	// Remove specified Figuur from the collection
	public boolean removeFiguur(Figuur figuur)
	{
		return collection.remove(figuur);
	}
	
	// Return the Figuur object specified by index
	public Figuur getFiguur(int index)
	{
		return collection.elementAt(index);
	}

	// Clear the collection
	public void clearAll()
	{
		collection.clear();
	}
	
	// Returns the current size of the collection
	public int sizeOf()
	{
		return collection.size();
	}

}
