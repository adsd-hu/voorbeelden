/*
 * Example code implementation of Figuur with:
 * -General coding and comments (working in progress)
 * -Abstraction
 * -Encapsulation
 * -Inheritance
 * -Polymorphism by using an abstract class
 * -Constructor overloading
 * -Method/function overriding
 * -Type safe collections
 *
 * Implemented in Java with the Singleton pattern
 *
 * See doc folder for the class diagram
 *
 * by G. Veldman
 */
package figuren;

import figuren.Figuur;

public class Program 
{
	public static void main(String[] args) 
	{
		System.out.println( "Java Programming! \n");
		
        // Get Singleton instance for Figuurlijst
        FigurenLijst figurenLijst = FigurenLijst.getInstance();

        Vierkant figuur1 = new Vierkant();
        figuur1.setX(5);
        figuur1.setOmschrijving("Vierkant");
        figurenLijst.addFiguur(figuur1);

        Cilinder figuur2 = new Cilinder(10,3);
        Cilinder figuur3 = new Cilinder(25,13);
        figuur2.setOmschrijving("Cilinder 1");
        figuur3.setOmschrijving("Cilinder 2");            
        
        figurenLijst.addFiguur(figuur2);
        figurenLijst.addFiguur(figuur3);          

        System.out.printf("Figuur 1, %s: %.2f bij %.2f \n", 
                            figuur1.getOmschrijving(), 
                            figuur1.getX(), figuur1.getX());
        System.out.printf("Figuur 2, %s: %.2f bij %.2f \n", 
                            figuur2.getOmschrijving(), 
                            figuur2.getH(), 
                            figuur2.getR());
        
        // Iterate figurenLijst
        // Gebruik generic type Figuur and polymorph methods
        for(int index=0; index < figurenLijst.sizeOf(); index++)
        {   
            Figuur huidigFiguur = figurenLijst.getFiguur(index);
            
            System.out.printf("Oppervlakte %s is: %.2f \n", 
                               huidigFiguur.getOmschrijving(),
                               huidigFiguur.berekenOppervlakte());
        }
	}

}
