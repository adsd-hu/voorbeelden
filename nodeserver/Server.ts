/// A Simple server in Typescript and Express
/// G. Veldman @ dec 2020

import * as express from 'express';

export class Server {
	private ipAddress: string;
	private portNumber: number;
	private expressServer;

	constructor();
	constructor(ipAddress: string, portNumber: number);
	constructor(ipAddress?: any, portNumber?: any)
	{
		this.ipAddress = ipAddress;
		this.portNumber = portNumber;
		this.expressServer = express();
	}

	public setIpAddress(ipAddress: string)
	{
		this.ipAddress = ipAddress;
	}

	public setPortNumber(portNumber: number)
	{
		this.portNumber = portNumber;
	}

	public launchServer()
   	{
   		/// Handle the GET request and send a response
 		this.expressServer.get('/', (request, response) => {
 			response.send("[!] Nothing todo arround here...");
 		})

 		this.expressServer.get('/Name/', (request, response) => {
 			response.send("You requested " + request.query.firstname + " " + request.query.lastname);
 		});

 		/// Start the server listener and print specified data to the console
 		this.expressServer.listen(this.portNumber, this.ipAddress, () => {
 			console.log("Server listening on " + this.ipAddress + ": " + this.portNumber + " for requests :-)")
 		});
   	}

}