/**
 * A Simple server in Typescript and Express
 * G. Veldman @ mar 2021
 *
 * Run example in a client or webbrowser App
 *		localhost:3000/
 * 	localhost:3000/Name/?firstname=Lisa&lastname=Brown
 */
import {Server} from "./Server";
import {System} from "./System";

class Program {

	static main() 
	{
		var port:number = Number(System.consoleReadline("Enter a port to launch the server on [3000]: "));

		/// Create instance of Server as myServer with specified location
		let myServer = new Server("localhost", port)

		/// Run myServer
		myServer.launchServer();

	}

}

// Start the application
Program.main();

