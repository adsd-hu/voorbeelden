import java.util.ArrayList;

import java.io.FileWriter;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

class Docent {
    public Docent()
    {

    }
    public Docent(String naam, int leeftijd)
    {
        this.naam = naam;
        this.leeftijd = leeftijd;
    }
    public String naam;
    public int leeftijd;
    public Docent geeftCollegeMet;
}

public class Main {
    @SuppressWarnings("unchecked")
    public static void main(String[] args) 
    {
        ArrayList<Docent> docenten = new ArrayList<Docent>();
        JSONArray docentenObjectLijst = new JSONArray();

        Docent docent1 = new Docent("Gerrit", 46);
        docenten.add(docent1);

        Docent docent2 = new Docent("Charlie", 38);
        docenten.add(docent2);


        for (int i = 0; i < docenten.size(); i++) {
            Docent huidigeDocent = docenten.get(i);

            JSONObject docentDetails = new JSONObject();
            docentDetails.put("naam", huidigeDocent.naam);
            docentDetails.put("leeftijd", huidigeDocent.leeftijd);

            docentenObjectLijst.add(docentDetails);
        } 

        JSONObject docentenObject = new JSONObject();
        docentenObject.put("docenten", docentenObjectLijst);

        try (FileWriter fileWriter = new FileWriter("data.json")) {
            fileWriter.write(docentenObject.toJSONString());
            fileWriter.flush();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
